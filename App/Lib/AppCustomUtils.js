var _ = require('lodash');

// apakah screen yang skr active adalah screen Signin
// bertujuan untuk menghindari componentWillReceiveProps dipanggil ktk screen sdh tdk aktif
export function isActiveScreen(props, screen) {
  if(!props.hasOwnProperty('active_screen')) return true
  if(!_.isNull(props.active_screen) && props.active_screen !== screen)
    return false
  return true
}

export function convertError(errorData) {
  if(_.isNull(errorData) || _.isEmpty(errorData) || _.isNil(errorData)) return ''
  let errorOutput = ''
  let error = ''
  if(_.isObject(errorData)) {
    if(errorData.hasOwnProperty('message')) error = errorData.message
    else if(errorData.hasOwnProperty('errors')) error = errorData.errors
    else if(errorData.hasOwnProperty('error')) error = errorData.error
    if(_.isObject(error)) {
      _.forEach(error, (value, key) => {
        errorOutput = errorOutput + value + '\n'
      })
    } else {
      errorOutput = error
    }
  } else {
    errorOutput = errorData
  }
  errorOutput = _.trimEnd(errorOutput, ',')
  return errorOutput
}


export const filterItem = [{
  column: 'NAMA_BANGUNAN',
  type: 'text',
  value: ''
},
{
  column: 'JENIS_BANGUNAN',
  type: 'dropdown',
  value: ''
}]

export function filterData(filters, items) {
  return items.filter((item) =>
    filters.every((filter) => {
      if(filter.type === 'dropdown' && filter.value == 'Semua') {         // utk parameter tipe dropdown, jika pilih 'semua' maka tdk perlu filter
        return true
      } else {                                                            // utk parameter tipe lain, periksa apa cocok dgn current data atau tidak
        return item[filter.column].toLowerCase().includes(filter.value.toLowerCase())
      }
    }
  ))
}


// ekstrak filename dari full path (berlaku utk / dan \)
export function getFilenameFromPath(input) {
  return input.replace(/^.*[\\\/]/, '')
}

// ekstrak filetype utk protokol HTML berdasarkan nama file
export function getFileType(input) {
  if(input.includes('.')) {         // jika input adalah filename dg ekstensi
    input = input.split('.').pop()  // ekstrak ekstensi dari filename
  }

  let output = ''
  switch(input.toLowerCase()) {
    // image file
    case 'png': output = 'image/png'; break;
    case 'jpg': output = 'image/jpg'; break;
    case 'jpeg': output = 'image/jpeg'; break;
    case 'gif': output = 'image/gif'; break;
    case 'bmp': output = 'image/x-ms-bmp'; break;
    case 'wbmp': output = 'image/vnd.wap.wbmp'; break;
    case 'webp': output = 'image/webp'; break;

    case 'txt': output = 'text/plain'; break;
    case 'htm': output = 'text/html'; break;
    case 'html': output = 'text/html'; break;
    case 'pdf': output = 'application/pdf'; break;
    case 'doc': output = 'application/msword'; break;
    case 'docx': output = 'application/msword'; break;
    case 'xls': output = 'application/vnd.ms-excel'; break;
    case 'xlsx': output = 'application/vnd.ms-excel'; break;
    case 'ppt': output = 'application/mspowerpoint'; break;
    case 'pptx': output = 'application/mspowerpoint'; break;
    case 'flac': output = 'audio/flac'; break;
    case 'zip': output = 'application/zip'; break;
    case 'mpg': output = 'video/mp2p'; break;
    case 'mpeg': output = 'video/mp2p'; break;
  }
  return output
}

import moment from 'moment'
import 'moment/locale/id'
var _ = require('lodash')

export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_READABLE_FORMAT = 'DD MMMM YYYY';
export const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export function getCurrentDate() {
  return moment()
}

export function isSameDay(_date1, _date2) {
  let date1 = moment(_date1).format(DATE_TIME_FORMAT)
  let date2 = moment(_date2).format(DATE_TIME_FORMAT)
  return moment(date1).diff(date2, 'days') == 0 ? true : false
}

export function stringToDate(date) {
  return moment(date).format(DATE_FORMAT);
}

export function getIntervalTime(old, current) {
  return moment(old).from(moment(current));
}

// output: 3 hari yang lalu, 22 hari yang lalu, dst
export function getIntervalTimeToday(old) {
  return moment(old).from(moment());
}

// output: Hari ini, Kemarin, 22 Juli, 21 Juli, dst
export function getIntervalDateToday(old, _format) {
  let a = moment(old)
  let b = moment()
  let diff = a.diff(b, 'days')
  let format = _.isNil(_format) ? 'DD MMM' : _format
  if(moment(old).isSame(moment(), 'year')) {  // jika tahunnya sama
    switch(diff) {
      case 0: return 'Hari ini'; break;
      case -1: return 'Kemarin'; break;
      default: return exports.getReadableDate(old, null, format)
    }
  } else {    // jika tahunnya tidak sama
    let isContainYear = format.includes('Y') || format.includes('y') ? true : false
    if(!isContainYear) format += ' YY'
    return exports.getReadableDate(old, null, format)
  }
}

export function getReadableDate(date, locale, format) {
  if(date === 'undefined' || date === '' || date === null || date === '0000-00-00 00:00:00')
    return '-';
  else return moment(date).locale(locale ? locale : 'id').format(format ? format : DATE_READABLE_FORMAT);
}

export function getReadableWeekdays(date) {
  if(date === 'undefined' || date === '' || date === null || date === '0000-00-00 00:00:00')
    return '';
  else return translateWeekdays(moment(date).isoWeekday());
}

function translateWeekdays(day) {
  switch(day) {
    case 1: return 'Senin';
    case 2: return 'Selasa';
    case 3: return 'Rabu';
    case 4: return 'Kamis';
    case 5: return 'Jumat';
    case 6: return 'Sabtu';
    case 7: return 'Minggu';
  }
}

export function isValidDate(str) {
  if(str === '0000-00-00 00:00:00') return true;
  return moment(str, DATE_TIME_FORMAT, true).isValid();
}

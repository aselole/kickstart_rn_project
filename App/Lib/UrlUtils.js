
export function getFirstParam(param) {
  let index = param.indexOf(',')
  if(index > -1) {
    return param.slice(0, index)
  }
  return param
}

var _ = require('lodash')

// parse HTML style text to array of string
export function parseHTML(input) {
  return _.remove(input.split(/<[^>]*>?/g), (item) => !_.isEmpty(item) && (/\S/.test(item)))
}

// opsi1: /<(?:.|\n)*?>/gm
// opsi2: /<[^>]*>?/g

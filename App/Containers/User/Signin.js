import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../../Themes/'
import UserAction from '../../Redux/UserRedux'
import { debugApi } from '../../Lib/NetworkUtils'
import { TextInputIcon, ButtonBox } from '../../Components/'
var _ = require('lodash');

// Styles
import styles from './Style'
import stylesTheme from '../../Themes/StyleSub'

class Login extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
    this.state = {
      username: '', password: '',
      error: false, error_info: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error_info, nextProps.error_info)) {
      this._onShowError(nextProps.error_info)
    }
  }

  _onShowError(error) {
    let errorText = ''
    if(_.isObject(error)) {
      _.forEach(error, (value, key) => {
        errorText = errorText + value + '\n'
      })
    } else {
      errorText = error
    }
    errorText = _.trimEnd(errorText, ',')
    this.setState({error_info: errorText})
  }

  onSignin() {
    this.props.signin(this.state.username, this.state.password)
  }

  onRegister() {
    this.props.navigation.navigate('Register', {item: 't'});
  }

  onForgot() {
    this.props.navigation.navigate('ForgotPassword', {item: 't'});
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <TextInputIcon
            onChangeText={(username) => this.setState({username})} />
          <TextInputIcon
            onChangeText={(password) => this.setState({password})} />
          <ButtonBox
            onPress={() => this.onSignin()}
            title='signin'/>
          <Text>{this.state.error_info}</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error_info: state.user.error_info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signin: (username, password) => dispatch(UserAction.signin(username, password)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes/'
import React, { Dimensions, PixelRatio } from 'react-native'
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clear,
    justifyContent: 'center',
  },
  button: {
    width: 305,
    height: 30,
    margin: Metrics.section,
    justifyContent: 'center',
  },
  textInput: {

  },
  textRegisterTitle: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    marginTop: 15,
  },
  textRegister: {
    textAlign: 'center', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  textForgotPass: {
    textAlign: 'right', // <-- the magic
    fontSize: 14,
    color: 'blue',
    marginTop: 2,
  },
  dropdown: {
    height:50,
    width:null,
    borderColor: 'lightgray',
    borderRadius: 2,
    backgroundColor: 'white',
    justifyContent: 'center'
  },
})

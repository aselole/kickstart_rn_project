import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from '../Themes/'
import UserAction from '../Redux/UserRedux'
import { debugApi } from '../Lib/NetworkUtils'
import { TextInputIcon, ButtonBox } from '../Components/'
import FlexImage from 'react-native-flex-image'
var _ = require('lodash');

class Tentang extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
  }

  componentDidMount() {
    if(this.props.signed_in) {
      this.props.navigation.navigate('Dashboard')
    } else {
      this.props.navigation.navigate('Signin')
    }
  }

  render () {
    return (
      <View style={{ flex:1 }}>
        {/*<FlexImage source={Images.logo} />*/}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    signed_in: state.user.ok_signin,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tentang)

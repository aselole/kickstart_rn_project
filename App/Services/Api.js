import apisauce from 'apisauce'
import { jsonToFormEncodedCaps } from '../Lib/JsonUtils'
import AppConfig from '../Config/AppConfig'
import { store } from '../Containers/App'       // how to access redux from non-react component
import { isNull } from '../Lib/CheckUtils'

const create = (baseURL = AppConfig.baseServerURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    timeout: 30000,
    onUploadProgress: progress => {                 // listener untuk progress upload attachment
      // store.dispatch(UploadActions.dataUploadProgress(progress.loaded, progress.total, store.getState().upload.upload_sequence[0]))   // how to access redux from non-react component
    }
  })

  api.addRequestTransform(request => {  // rubah format request parameter
    request.data = jsonToFormEncodedCaps(request.data)
  })

  api.addResponseTransform(response => {  // rubah format response parameter
    if(isNull(response.data)) response.ok = false
    else if(response.data.code === 500) response.ok = false
  })

  const signin = (payload) => api.post('api/login/micomm', payload)
  const signup = (payload) => api.post('UserGobang/signup', payload)

  return {
    signin,
    signup,
  }
}

export default {
  create
}

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  reset: null,
  uploadFotoUser: ['token', 'data'],
  onUploadSuccess: ['response'],
  onUploadFailed: ['error_info'],
})

export const UploadTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  is_fetching: false,
  token: null,
  data: null,
  response: null,
  error_info: null,
})

/* ------------- Reducers ------------- */

export const reset = ( state ) => INITIAL_STATE

export const uploadFotoUser = ( state, { token, data }) =>
  state.merge({
    token, data,
    response: null,
    error_info: null,
    is_fetching: true
  })

export const onUploadSuccess = ( state, { response }) =>
  state.merge({
    response,
    is_fetching: false
  })

export const onUploadFailed = ( state, { error_info }) =>
  state.merge({
    error_info,
    is_fetching: false
  })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET]: reset,
  [Types.UPLOAD_FOTO_USER]: uploadFotoUser,
  [Types.ON_UPLOAD_SUCCESS]: onUploadSuccess,
  [Types.ON_UPLOAD_FAILED]: onUploadFailed,
})

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  resetList: null,
  getPartner: ['token'],
  getPartnerDetail: ['token', 'id'],
  getDeals: ['token'],
  getDealsDetail: ['token', 'id'],
  getBerita: ['token'],
  getBeritaDetail: ['token', 'id'],
  getTrainer: ['token'],
  getEventGlobal: ['token'],
  getEventMember: ['token'],
  getEventDetail: ['token', 'id'],
  getCommunityMember: ['token'],
  getObrolanComm: ['token'],
  getObrolanCommDetail: ['token', 'id'],
  getObrolanPrivate: ['token'],
  getObrolanPrivateDetail: ['token', 'id'],

  onDataReceived: ['data'],
  onDataError: ['response'],
  onArrayDataReceived: ['data_array', 'index_array'],
  onArrayDataError: ['response', 'index_array'],
  onDetailReceived: ['detail'],
  onDetailError: ['error_info'],
  onListClicked: ['id'],
  onListItemClicked: ['data_selected'],
})

export const ListTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  id: null,
  data: null,
  data_selected: null,
  index_array: { 0: null, 1: null },
  data_array: { 0: null, 1: null },
  error_title_array: { 0: null, 1: null },
  error_info_array: { 0: null, 1: null },
  is_fetching_array: { 0: null, 1: null },
  detail: null,
  error_info: null,
  error_title: null,
  token: null,
  is_fetching: false,
})

/* ------------- Reducers ------------- */

export const resetList = (state) => INITIAL_STATE

export const getPartner = (state, { token }) =>
  state.merge({ token, data: null, error_info: null, error_title: null, is_fetching: true })

export const getPartnerDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const getDeals = (state, { token }) =>
  state.merge({ token, data: null, error_info: null, error_title: null, is_fetching: true })

export const getDealsDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const getBerita = (state, { token }) =>
  state.merge({ token, data: null, error_info: null, error_title: null, is_fetching: true })

export const getBeritaDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const getTrainer = (state, { token }) =>
  state.merge({ token, data: null, error_info: null, error_title: null, is_fetching: true })

export const getObrolanComm = (state, { token }) =>
  state.merge({
    token,
    data_array: { 0: null, 1: null },
    error_info_array: { 0: null, 1: null },
    error_title_array: { 0: null, 1: null },
    is_fetching_array: state.is_fetching_array.set([0], true)
  })

export const getObrolanCommDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const getObrolanPrivate = (state, { token }) =>
  state.merge({
    token,
    data_array: { 0: null, 1: null },
    error_info_array: { 0: null, 1: null },
    error_title_array: { 0: null, 1: null },
    is_fetching_array: state.is_fetching_array.set([1], true)
  })

export const getObrolanPrivateDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const onDataReceived = (state, { data }) =>
  state.merge({ data, is_fetching: false })

export const onDataError = (state, { response }) =>
  state.merge({ error_info: response.data, error_title: response.problem, is_fetching: false })

export const onDetailReceived = (state, { detail }) =>
  state.merge({ detail, is_fetching: false })

export const onDetailError = (state, { error_info }) =>
  state.merge({ error_info, is_fetching: false })

export const onListClicked = (state, { id }) =>
  state.merge({ id })

export const onListItemClicked = (state, { data_selected }) =>
  state.merge({ data_selected })

export const getEventGlobal = (state, { token }) =>
  state.merge({
    token,
    data_array: { 0: null, 1: null },
    error_info_array: { 0: null, 1: null },
    error_title_array: { 0: null, 1: null },
    is_fetching_array: state.is_fetching_array.set([0], true)
  })

export const getEventMember = (state, { token }) =>
  state.merge({
    token,
    data_array: { 0: null, 1: null },
    error_info_array: { 0: null, 1: null },
    error_title_array: { 0: null, 1: null },
    is_fetching_array: state.is_fetching_array.set([1], true)
  })

export const getEventDetail = (state, { token, id }) =>
  state.merge({ token, detail: null, is_fetching: true })

export const onArrayDataReceived = (state, { data_array, index_array }) =>
  state.merge({
    data_array: state.data_array.set([index_array], data_array),
    is_fetching_array: state.is_fetching_array.set([index_array], false)
  })

export const onArrayDataError = (state, { response, index_array }) =>
  state.merge({
    error_info_array: state.error_info_array.set([index_array], response.data),
    error_title_array: state.error_title_array.set([index_array], response.problem),
    is_fetching_array: state.is_fetching_array.set([index_array], false)
  })

export const getCommunityMember = (state, { token }) =>
  state.merge({ token, data: null, error_info: null, error_title: null, is_fetching: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_LIST]: resetList,
  [Types.GET_PARTNER]: getPartner,
  [Types.GET_PARTNER_DETAIL]: getPartnerDetail,
  [Types.GET_DEALS]: getDeals,
  [Types.GET_DEALS_DETAIL]: getDealsDetail,
  [Types.GET_BERITA]: getBerita,
  [Types.GET_BERITA_DETAIL]: getBeritaDetail,
  [Types.GET_TRAINER]: getTrainer,
  [Types.GET_EVENT_GLOBAL]: getEventGlobal,
  [Types.GET_EVENT_MEMBER]: getEventMember,
  [Types.GET_EVENT_DETAIL]: getEventDetail,
  [Types.GET_COMMUNITY_MEMBER]: getCommunityMember,
  [Types.GET_OBROLAN_COMM]: getObrolanComm,
  [Types.GET_OBROLAN_COMM_DETAIL]: getObrolanCommDetail,
  [Types.GET_OBROLAN_PRIVATE]: getObrolanPrivate,
  [Types.GET_OBROLAN_PRIVATE_DETAIL]: getObrolanPrivateDetail,
  [Types.ON_DATA_RECEIVED]: onDataReceived,
  [Types.ON_DATA_ERROR]: onDataError,
  [Types.ON_ARRAY_DATA_RECEIVED]: onArrayDataReceived,
  [Types.ON_ARRAY_DATA_ERROR]: onArrayDataError,
  [Types.ON_DETAIL_RECEIVED]: onDetailReceived,
  [Types.ON_DETAIL_ERROR]: onDetailError,
  [Types.ON_LIST_CLICKED]: onListClicked,
  [Types.ON_LIST_ITEM_CLICKED]: onListItemClicked,
})

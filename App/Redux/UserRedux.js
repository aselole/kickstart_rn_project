import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  signup: ['username', 'password'],
  signupOk: ['id_user'],
  signupFail: ['error_info'],
  signin: ['username', 'password'],
  signinOk: ['data_user'],
  signinFail: ['error_info'],
  signout: null,
  forgotPass: ['username'],
  forgotPassOk: ['id_user'],
  forgotPassFail: ['error_info'],
  updatePass: ['id_user', 'password'],
  updatePassOk: null,
  updatePassFail: ['error_info'],
  reset: null,
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  error_info: '',
  ok_signin: null,
  ok_signup: null,
  ok_forgot_pass: null,
  ok_update_pass: null,
  username: '',
  password: '',
  data_user: {},
})

/* ------------- Reducers ------------- */

export const signin = (state, { username, password }) =>
  state.merge({ fetching: true, ok_signin: null, username, password })

export const signinOk = (state, { data_user }) =>
  state.merge({ fetching: false, ok_signin: true, data_user })

export const signinFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_signin: false, data: null, username: null, password: null, error_info })


export const signout = (state) => INITIAL_STATE


export const signup = (state, { username, password }) =>
  state.merge({ fetching: true, ok_signup: null, username, password })

export const signupOk = (state, { id_user }) =>
  state.merge({ fetching: false, ok_signup: true, id_user })

export const signupFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_signup: false, error_info })


export const forgotPass = (state, { username }) =>
  state.merge({ fetching: true, ok_forgot_pass: null, username })

export const forgotPassOk = (state, { id_user }) =>
  state.merge({ fetching: false, ok_forgot_pass: true })

export const forgotPassFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_forgot_pass: false, error_info })


export const updatePass = (state, { id_user, password }) =>
  state.merge({ fetching: true, ok_update_pass: null, id_user, password })

export const updatePassOk = (state, { }) =>
  state.merge({ fetching: false, ok_update_pass: true })

export const updatePassFail = (state, { error_info }) =>
  state.merge({ fetching: false, ok_update_pass: false, error_info })


export const reset = (state) => INITIAL_STATE


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGNIN]: signin,
  [Types.SIGNIN_OK]: signinOk,
  [Types.SIGNIN_FAIL]: signinFail,
  [Types.SIGNOUT]: signout,
  [Types.SIGNUP]: signup,
  [Types.SIGNUP_OK]: signupOk,
  [Types.SIGNUP_FAIL]: signupFail,
  [Types.FORGOT_PASS]: forgotPass,
  [Types.FORGOT_PASS_OK]: forgotPassOk,
  [Types.FORGOT_PASS_FAIL]: forgotPassFail,
  [Types.UPDATE_PASS]: updatePass,
  [Types.UPDATE_PASS_OK]: updatePassOk,
  [Types.UPDATE_PASS_FAIL]: updatePassFail,
  [Types.RESET]: reset,
})

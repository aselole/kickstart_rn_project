import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from '../Services/Api'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { UserTypes } from '../Redux/UserRedux'

/* ------------- Sagas ------------- */

import { signin, signup } from './UserSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()

/* ------------- Connect Types To Sagas ------------- */

//root
export default function * root() {
  try {
    yield [
      // user
      takeLatest(UserTypes.SIGNIN, signin, api),
      takeLatest(UserTypes.SIGNUP, signup, api),
    ]
  }
  catch(error) {
    console.tron.log('error saga '+error)
  }
}

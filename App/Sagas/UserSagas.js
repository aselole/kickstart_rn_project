import { call, put } from 'redux-saga/effects'
import UserActions from '../Redux/UserRedux'
import { Strings } from '../Themes/'

export function * signin (api, action) {
  const response = yield call(api.signin, action)
  console.tron.display({
    name: 'UserSagas',
    value: {
      response: response
    }
  })

  if (response.ok) {
    yield put(UserActions.signinOk(response.data.result[0]))
  } else {
    yield put(UserActions.signinFail(response.data.errors))
  }
}

export function * signup (api, action) {
  const response = yield call(api.signup, action)

  if (response.ok) {
    if(response.data !== null) {
      switch(response.data.code) {
        case 200:
          yield put(UserActions.signupOk(response.data.id_detail))
          return;
        case 50:
          yield put(UserActions.signupFail(Strings.fail_hp_empty))
          return;
        case 100:
          yield put(UserActions.signupFail(Strings.fail_hp_used))
          return;
        case 150:
          yield put(UserActions.signupFail(Strings.fail_username))
          return;
        default:
          yield put(UserActions.signupFail(Strings.fail_signup))
          return;
      }
    }
  }
  yield put(UserActions.signupFail(Strings.fail_signup))
}

export function * forgotPass (api, action) {
  const response = yield call(api.usernameCheck, action)

  if(response.ok) {
    yield put(UserActions.forgotPassOk(response.data.id_detail, response.data.hp))
  } else {
    yield put(UserActions.forgotPassFail(response))
  }
}

export function * updatePass (api, action) {
  const response = yield call(api.updatePassword, action)

  if(response.ok) {
    yield put(UserActions.updatePassOk())
  } else {
    yield put(UserActions.updatePassFail(response))
  }
}

import { Platform, AsyncStorage, AppState } from 'react-native'
import FCM, {
  FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType,
  NotificationActionType, NotificationActionOption, NotificationCategoryOption
} from "react-native-fcm"
import { store } from '../Containers/App'
import ChatActions from '../Redux/ChatRedux'
import DebugConfig from '../Config/DebugConfig'

// these callback will be triggered only when app is foreground or background
export function registerAppListener(onNotificationClicked) {
  FCM.on(FCMEvent.Notification, notif => {
    console.log('Notification - Remote Notification received '+ JSON.stringify(notif))
    if(!notif.hasOwnProperty('is_local')) {  // remote notification arrived
      if(notif.jenis !== 'private_issue_read') {  // jika bukan notifikasi yg perlu ditampilkan
        if (DebugConfig.useReactotron) {
          console.tron.display({
            name: 'Notification - Remote Notification received',
            value: { notif: notif }
          })
        }
        FCM.presentLocalNotification({
          channel: 'default',
          id: notif.id,
          title: notif.title,
          body: notif.body,
          jenis: notif.jenis,
          created_at: notif.created_at,
          sound: 'bell.mp3',
          priority: 'high',
          icon: "ic_launcher",
          show_in_foreground: true, // notification when app is in foreground (local & remote)
          is_local: true,
        })
        store.dispatch(
          ChatActions.onMessageArrived({
            id: notif.id,
            title: notif.title,
            body: notif.body,
            created_at: notif.created_at
          })
        )
      } else {  // notifikasi untuk mengupdate status read/belum untuk pesan yg telah dikirim
        if (DebugConfig.useReactotron) {
          console.tron.display({
            name: 'Notification - Remote Notification Read received',
            value: { notif: notif }
          })
        }
        store.dispatch(ChatActions.onMessageRead(notif.id))
      }
    } else {   // local notification arrived
      if (DebugConfig.useReactotron) {
        console.tron.display({
          name: 'Notification - Local Notification received',
          value: { notif: notif }
        })
      }
    }

    if(Platform.OS ==='ios' && notif._notificationType === NotificationType.WillPresent && !notif.local_notification){
      // this notification is only to decide if you want to show the notification when user if in foreground.
      // usually you can ignore it. just decide to show or not.
      notif.finish(WillPresentNotificationResult.All)
      return;
    }

    if(notif.opened_from_tray) {
      if (DebugConfig.useReactotron) {
        console.tron.display({
          name: 'Notification - Notification start click',
          value: { notif: notif }
        })
      }
      setTimeout(() => onNotificationClicked(notif), 500)
    }

    if(Platform.OS ==='ios') {
      // optional
      // iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
      // This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
      // notif._notificationType is available for iOS platfrom
      switch(notif._notificationType) {
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          // this type of notificaiton will be called only when you are in foreground.
          // if it is a remote notification, don't do any app logic here. Another notification callback will be triggered with type NotificationType.Remote
          break;
      }
    }
  })

  FCM.on(FCMEvent.RefreshToken, token => {
    if (DebugConfig.useReactotron) {
      console.tron.display({
        name: 'Notification - TOKEN refresh',
        value: { token: token }
      })
    }
  })

  FCM.enableDirectChannel()
  FCM.on(FCMEvent.DirectChannelConnectionChanged, (data) => {
    if (DebugConfig.useReactotron) {
      console.tron.display({
        name: 'Notification - Direct Channel Connection Changed',
        value: { data: data }
      })
    }
  })
  setTimeout(function() {
    FCM.isDirectChannelEstablished().then(d => {
      if (DebugConfig.useReactotron) {
        console.tron.display({
          name: 'Notification - Direct Channel Connected',
          value: { info: d }
        })
      }
    })
  }, 1000)
}

// these callback will be triggered even when app is killed
export function registerKilledListener(){
  if(Platform.OS === 'ios') return
  FCM.on(FCMEvent.Notification, notif => {
    if (DebugConfig.useReactotron) {
      console.tron.display({
        name: 'registerKilledListener onreceive notification',
        value: { notif: notif }
      })
    }
    console.log('registerKilledListener onreceive notification '+ JSON.stringify(notif))
    AsyncStorage.setItem('lastNotification', JSON.stringify(notif));
    if(notif.opened_from_tray){
      setTimeout(()=>{
        if(notif._actionIdentifier === 'reply'){
          if(AppState.currentState !== 'background'){
            if (DebugConfig.useReactotron) {
              console.tron.display({
                name: 'registerKilledListener not in background',
                value: {
                  notif: notif,
                  userText: JSON.stringify(notif._userText),
                }
              })
              console.log('registerKilledListener not in background '+ JSON.stringify(notif._userText))
            }
          } else {
            if (DebugConfig.useReactotron) {
              console.tron.display({
                name: 'registerKilledListener in background',
                value: {
                  notif: notif,
                  userText: JSON.stringify(notif._userText),
                }
              })
              console.log('registerKilledListener in background '+ JSON.stringify(notif._userText))
            }
            AsyncStorage.setItem('lastMessage', JSON.stringify(notif._userText));
          }
        }
        if(notif._actionIdentifier === 'view'){
          if (DebugConfig.useReactotron) {
            console.tron.display({
              name: 'registerKilledListener User clicked View in App',
              value: { notif: notif }
            })
            console.log('registerKilledListener User clicked View in App '+ JSON.stringify(notif))
          }
        }
        if(notif._actionIdentifier === 'dismiss'){
          if (DebugConfig.useReactotron) {
            console.tron.display({
              name: 'registerKilledListener User clicked Dismiss',
              value: { notif: notif }
            })
            console.log('registerKilledListener User clicked Dismiss '+ JSON.stringify(notif))
          }
        }
      }, 1000)
    }
  });
}

AsyncStorage.getItem('lastNotification').then(data=>{
  if(data){
    // if notification arrives when app is killed, it should still be logged here
    if (DebugConfig.useReactotron) console.log('last notification', JSON.parse(data));
    AsyncStorage.removeItem('lastNotification');
  }
})

AsyncStorage.getItem('lastMessage').then(data=>{
  if(data){
    // if notification arrives when app is killed, it should still be logged here
    if (DebugConfig.useReactotron) console.log('last message', JSON.parse(data));
    AsyncStorage.removeItem('lastMessage');
  }
})

// these callback will be triggered on app restart after killed while notification arrived
export function recoverBackFromDeadNotification(onNotificationClicked) {
  if(Platform.OS === 'ios') return
  FCM.getInitialNotification().then(notif => {
    if (DebugConfig.useReactotron) {
      console.tron.display({
        name: 'recoverBackFromDeadNotification',
        value: { notif: notif }
      })
    }
    if(notif.hasOwnProperty('jenis')) {
      setTimeout(() => onNotificationClicked(notif), 500)
    }
  });
}

export async function requestPermissions() {
  if(Platform.OS === 'ios') return
  try {
    let result = await FCM.requestPermissions({
      badge: false,
      sound: true,
      alert: true
    });
  } catch (e) {
    alert(e)
  }
}

import FCM from 'react-native-fcm'

export function createNotificationChannel() {
  FCM.createNotificationChannel({
    id: 'default',
    name: 'Default',
    description: 'the only channel in this app',
    priority: 'high'
  })
}

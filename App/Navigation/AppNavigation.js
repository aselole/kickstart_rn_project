import React, { Component } from 'react'
import { Image } from 'react-native'
import { StackNavigator, TabNavigator } from 'react-navigation'
import { withMappedNavigationProps } from 'react-navigation-props-mapper'

import Splash from '../Containers/Splash'
import Signin from '../Containers/User/Signin'
import { Colors, Images, Strings } from '../Themes/'
import styles from './Style'

export const PrimaryNav = StackNavigator({
    Splash: { screen: withMappedNavigationProps(Splash) },
    Signin: { screen: withMappedNavigationProps(Signin) },
  }, {
    initialRouteName: 'Splash',
    headerMode: 'screen',
})

export default PrimaryNav

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, StyleSheet, View, Text,
  Image, TextInput, TouchableHighlight, TouchableOpacity
} from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'
import Modal from 'react-native-modal'
import * as Progress from 'react-native-progress'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
var _ = require('lodash')

export default class LoadingOverlay extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      title: '',
      progress: 0
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.title, nextProps.title) && !_.isEmpty(nextProps.title)) {
      this.setState({ title: nextProps.title })
    }
    if(!_.isEqual(this.props.isLoading, nextProps.isLoading)) {
      let isArray = Array.isArray(nextProps.isLoading)

      let usedIndex = isArray ? nextProps.isLoading.findIndex((item) => item) : 0
      let isLoading = isArray ? nextProps.isLoading.some((item) => item) : nextProps.isLoading
      let title = isArray ? nextProps.title[usedIndex] : nextProps.title

      this.setState({ isLoading: isLoading, title: title })
    }
    if(!_.isEqual(this.props.progress, nextProps.progress)) {
      this.setState({ progress: nextProps.progress })
    }
  }

  render () {
    return (
      <Modal
        isVisible={this.state.isLoading}
        backdropColor='gray'
        backdropOpacity={0.5}
        animationInTiming={1000}
        onModalHide={() => this.props.onModalHide()}>
        <View style={styles.container}>
          <View style={styles.loading}>
            { !this.props.isIndeterminate &&
              <Progress.Circle
                size={80}
                thickness={3}
                showsText={true}
                textStyle={[ styles.extraBold, {fontWeight:'bold', color:'red'} ]}
                strokeCap='round'
                color={this.props.loadingColor}
                progress={this.state.progress} />
            }
            { this.props.isIndeterminate && this.props.customIndeterminateLoading === 'wave' &&
              <WaveIndicator color={this.props.loadingColor} count={4} size={80} />
            }
            { this.props.isIndeterminate && this.props.customIndeterminateLoading === 'dot' &&
              <DotIndicator color={this.props.loadingColor} count={4} size={80} />
            }
          </View>
          <Text style={[ styles.title, this.props.titleStyle ]}>{this.state.title}</Text>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    width: deviceWidth - 100,
  },
  container: {
    flex:0.3,
    backgroundColor:'white',
    borderRadius: 15,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: 8
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  loading: {
    flex:3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'transparent'
  },
  title: {
    flex:1,
    alignSelf:'center',
    color:'grey'
  }
});

LoadingOverlay.propTypes = {
  title: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]),
  isLoading: PropTypes.oneOfType([ PropTypes.bool, PropTypes.array ]),
  progress: PropTypes.number,
  isIndeterminate: PropTypes.bool,
  customIndeterminateLoading: PropTypes.string,
  loadingColor: PropTypes.string,
  titleStyle: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  onModalHide: PropTypes.func,
}

LoadingOverlay.defaultProps = {
  isIndeterminate: true,
  loadingColor: 'red',
  customIndeterminateLoading: 'wave',
}

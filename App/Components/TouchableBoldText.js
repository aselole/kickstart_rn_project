import React, {Component} from "react"
import {Text, View, TouchableHighlight, StyleSheet } from "react-native"
import { Colors, Images, Servers, Strings } from '../Themes/'

import css from "../Themes/Style"

export default class IconInput extends Component {
    render() {
        return (
            <TouchableHighlight {...this.props}>
              <Text style={styles.mainText}>{this.props.title}</Text>
            </TouchableHighlight>
        );
    }

}

const styles = StyleSheet.create({
  "mainText": {
    "color": Colors.theme,
    "fontSize": 14,
    "fontWeight": "bold"
  },
})

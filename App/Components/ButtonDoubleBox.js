import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class ButtonDoubleBox extends Component {
  static propTypes = {
    title1: PropTypes.string,
    imgLeft1: PropTypes.number,
    imgRight1: PropTypes.number,
    title2: PropTypes.string,
    imgLeft2: PropTypes.number,
    imgRight2: PropTypes.number
  }

  render () {
    const { title1, imgLeft1, imgRight1, title2, imgLeft2, imgRight2 } = this.props
    return (
      <View style={[ stylesTheme.itemWhiteBox, {flexDirection: 'column'} ]}>
        <View style={styles.detailItemBox}>
          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={imgLeft1} style={{width:30, height:30}} />
            </View>
            <View style={{
              width: 0.7,
              height: 25,
              backgroundColor: 'rgb(229,229,229)'}}
            />
          </View>
          <View style={styles.centerInfo}>
            <Text style={[
              StyleSub.regularBold,
              StyleSub.normalText,
              StyleSub.greyColor,
            ]}>
            {title1 || '-'}
            </Text>
          </View>
          <View style={styles.rightCol}>
            <Image source={imgRight1} style={{width:30, height:30}} />
          </View>
        </View>
        <View style={[ styles.detailItemBox, {paddingTop: 0,} ]}>
          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={imgLeft2} style={{width:30, height:30}} />
            </View>
            <View style={{
              width: 0.7,
              height: 25,
              backgroundColor: 'rgb(229,229,229)'}}
            />
          </View>
          <View style={styles.centerInfo}>
            <Text style={[
              StyleSub.regularBold,
              StyleSub.normalText,
              StyleSub.greyColor,
            ]}>
            {title2 || '-'}
            </Text>
          </View>
          <View style={styles.rightCol}>
            <Image source={imgRight2} style={{width:30, height:30}} />
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  highlightBox: {
    borderRadius: 12,
  },
  centerInfo: {
    flex: 1,
  },
  leftCol: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 61,
  },
  icon: {
    width: 41,
  },
  rightCol: {
    width: 20,
    alignItems: 'center',
  },
});

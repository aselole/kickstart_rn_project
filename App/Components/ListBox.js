import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'
import { getReadableDate } from '../Lib/DateUtils'

export default class ProfileBox extends Component {
  static propTypes = {
    onPressItem: PropTypes.func.isRequired,
    nama: PropTypes.string,
    noRm: PropTypes.string,
    tglLahir: PropTypes.string,
    rumahSakit: PropTypes.string,
    tglDaftar: PropTypes.string,
    tglLastCheck: PropTypes.string,
  }

  componentDidMount() {
  }

  render () {
    const { onPressItem, nama, noRm, tglLahir, rumahSakit, tglDaftar, tglLastCheck } = this.props
    let tglLahirDisplay = getReadableDate(this.props.tglLahir)
    let tglDaftarDisplay = getReadableDate(this.props.tglDaftar)
    let tglLastCheckDisplay = getReadableDate(this.props.tglLastCheck)
    return (
      <View style={[ stylesTheme.itemWhiteBox, {marginTop:5} ]}>
        <TouchableOpacity
          underlayColor='transparent'
          style={styles.highlightBox}
          onPress={this.props.onPressItem}>
          <View style={styles.listItemBox}>

            <View style={styles.leftCol}>
              <Text style={[ stylesTheme.itemHeaderText, stylesTheme.blackColor, stylesTheme.mediumBold, {lineHeight: 25} ]}>
                {this.props.nama}
              </Text>
              <Text style={[ stylesTheme.greyColor, stylesTheme.regularBold, {fontSize: 14} ]}>
                {this.props.noRm}
              </Text>
              <Text style={[ stylesTheme.greyColor, stylesTheme.regularBold, {fontSize: 14} ]}>
                {tglLahirDisplay}
              </Text>
              <Text style={[ stylesTheme.greyColor, stylesTheme.regularBold, {fontSize: 14} ]}>
                {this.props.rumahSakit}
              </Text>
            </View>

            <View style={[ styles.rightCol ]}>
              <View style={[ styles.rightTop ]}>
                <Text style={[ stylesTheme.greyColor, stylesTheme.regularBold, {fontSize: 12, color: Colors.snow} ]}>
                  {tglDaftarDisplay}
                </Text>
              </View>
              <View style={[ styles.rightBottom ]}>
                <Text style={[ stylesTheme.greyColor, stylesTheme.regularBold, {fontSize: 12, color: Colors.snow} ]}>
                  {tglLastCheckDisplay}
                </Text>
              </View>
            </View>

          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  highlightBox: {
    borderRadius: 8,
  },
  listItemBox: {
    flexDirection: 'row',
  },
  leftCol: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 15,
  },
  rightCol: {
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'flex-end',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor:'#cccccc',
  },
  rightTop: {
    flex: 1,
    backgroundColor:'#1bac94',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderTopRightRadius: 8,
  },
  rightBottom: {
    flex: 1,
    backgroundColor:'#42a5f5',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderBottomRightRadius: 8,
  }
});

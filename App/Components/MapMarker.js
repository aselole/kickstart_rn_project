import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, ImageBackground } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'
import { Marker } from 'react-native-maps'

export default class MapMarker extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    return (
      <Marker
        identifier={`${this.props.id}`}
        key={this.props.id}
        coordinate={{
          latitude: parseFloat(this.props.location.latitude),
          longitude: parseFloat(this.props.location.longitude)
        }}>
        <View style={{flex:1, alignItems: 'center', justifyContent:'center'}}>
          <ImageBackground
            source={this.props.icon}
            style={{borderRadius:12.5, width:25, height:25}} />
          <Text
            style={{
              fontSize: 12,
              width: 80, position:'absolute', right:-83,
              textShadowColor: 'rgba(0, 0, 0, 0.1)',
              textShadowOffset: {width: -1, height: 1},
              textShadowRadius: 3,
              elevation: 3,
            }}
            numberOfLines={2}>
            {this.props.title}
          </Text>
        </View>
      </Marker>
    )
  }

}

const styles = StyleSheet.create({

});

MapMarker.propTypes = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  title: PropTypes.string,
  location: PropTypes.object,
  icon: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
}

MapMarker.defaultProps = { }

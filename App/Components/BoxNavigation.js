import React, { Component } from "react"
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image } from "react-native"
import PropTypes from 'prop-types'
import { Colors, Images, Servers, Strings } from '../Themes/'
import stylesTheme, { shadowOffsetWidth, shadowOffsetHeight, shadowRadius, shadowOpacity } from '../Themes/StyleSub';
import ConfirmDialog from './ConfirmDialog'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class BoxNavigation extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentIndex: props.currentIndex,
      isCurrentStart: this._getStartStatus(props.currentIndex),
      isCurrentFinish: this._getFinishStatus(props.currentIndex, props.totalIndex),
      confirm: 0,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.currentIndex !== nextProps.currentIndex) {
      this.setState({
        currentIndex: nextProps.currentIndex,
        isCurrentStart: this._getStartStatus(nextProps.currentIndex),
        isCurrentFinish: this._getFinishStatus(nextProps.currentIndex, this.props.totalIndex),
      })
    }
  }

  _getStartStatus(curIndex) {
    return curIndex === 0 ? true : false
  }

  _getFinishStatus(curIndex, totalIndex) {
    return curIndex === (totalIndex - 1) ? true : false
  }

  _onPressLeft = () => {
    if(!this.state.isCurrentStart) this.props.onPressLeft()
  }

  _onPressMiddle = () => {
    if(this.props.isExitEnabled) this.props.onPressMiddle()
  }

  _onPressRightConfirm = () => {
    this.setState((prev) => { return {confirm:prev.confirm+1} })
  }

  _onPressRight = () => {
    if(!this.state.isCurrentFinish || (this.state.isCurrentFinish && this.props.showFinish)) {
      if(this.props.isNeedConfirm) {
        this._onPressRightConfirm()
      } else {
        this.props.onPressRight()
      }
    }
  }

  render() {
    return (
      <View>
        <View style={{height:50, flexDirection:'row'}}>
          <TouchableOpacity style={{flex:1, backgroundColor:Colors.bgAccent}} onPress={this._onPressLeft.bind(this)}>
            { !this.state.isCurrentStart &&
              <View style={{flex:1, flexDirection:'row', alignItems:'center',}}>
                <Image resizeMode='contain' source={Images.prev} style={{flex:1, width:20, height:22}} />
                <Text style={{flex:1.5, color:Colors.bgWhite}}>HAL {this.state.currentIndex}</Text>
              </View>
            }
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1, backgroundColor:Colors.bgDark, alignItems:'center', justifyContent:'center'}}
            onPress={this._onPressMiddle.bind(this)}>
            <Text style={{color:Colors.bgWhite, textAlign:'center'}}>HOME</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1, backgroundColor:Colors.bgLight}} onPress={this._onPressRight}>
            { !this.state.isCurrentFinish &&
              <View style={{flex:1, flexDirection:'row', alignItems:'center',}}>
                <Text style={{flex:1.5, color:Colors.bgWhite, textAlign:'right'}}>HAL {this.state.currentIndex+2}</Text>
                <Image resizeMode='contain' source={Images.next} style={{flex:1, width:20, height:22}} />
              </View>
            }
            { this.state.isCurrentFinish && this.props.showFinish &&
              <View style={{flex:1, flexDirection:'row', alignItems:'center',}}>
                <Text style={{flex:1.5, color:Colors.bgWhite, textAlign:'center'}}>SELESAI</Text>
              </View>
            }
          </TouchableOpacity>
        </View>
        <ConfirmDialog
          isVisible={this.state.confirm}
          onPressOk={this.props.onPressRight}
          title={this.props.confirmTitle}
          message={this.props.confirmMessage}
          icon={this.props.confirmIcon} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  BoxNavigationBlue: {

  },
  buttonRectangularText: {

  },
})

BoxNavigation.propTypes = {
  onPressLeft: PropTypes.func,
  onPressMiddle: PropTypes.func,
  onPressRight: PropTypes.func,
  currentIndex: PropTypes.number,
  totalIndex: PropTypes.number,
  showFinish: PropTypes.bool,
  isNeedConfirm: PropTypes.bool,
  confirmTitle: PropTypes.string,
  confirmMessage: PropTypes.string,
  confirmIcon: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
  isExitEnabled: PropTypes.bool,
};

BoxNavigation.defaultProps = {
  currentIndex: 0,
  showFinish: false,
  isNeedConfirm: false,
  isExitEnabled: true,
};

import React, { Component } from 'react'
import { Platform, StyleSheet, Animated, View, TouchableOpacity, Text, Picker } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './Styles/BoxStyles'
import stylesTheme from '../Themes/StyleSub'

var UIPICKER_HEIGHT = 216;

export default class PickerCollapsible extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
      height: new Animated.Value(0)
    };
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.locals.isCollapseParent !== nextProps.locals.isCollapseParent) {
      Animated.timing(
        this.state.height,
        Object.assign(
          { toValue: this.state.isCollapsed ? UIPICKER_HEIGHT : 0 },
          { duration: 200 }
        )
      ).start();
      this.setState({ isCollapsed: !this.state.isCollapsed });
    }
  }

  toggleCollapsible() {
    Animated.timing(
      this.state.height,
      Object.assign(
        { toValue: this.state.isCollapsed ? UIPICKER_HEIGHT : 0 },
        { duration: 200 }
      )
    ).start();
    this.setState({ isCollapsed: !this.state.isCollapsed });
  }

  render() {
    let pickerContainer = styles.pickerContainerNormal;
    let pickerContainerOpen = styles.pickerContainerOpen;
    let selectStyle = styles.selectNormal;
    let touchableStyle = styles.pickerTouchableNormal;
    let touchableStyleActive = styles.pickerTouchableActive;
    let pickerValue = styles.pickerValueNormal;

    if(!this.props.locals.options || this.props.locals.options.length == 0) {
      return null;
    }

    const options = this.props.locals.options.map((item, key) => (
      <Picker.Item key={item.ID} value={item.ID} label={item.NAMA} />
    ));
    const selectedOption = this.props.locals.selectedValue == 0 ?
      this.props.locals.options[0] :
      this.props.locals.options.find(
        option => option.ID === this.props.locals.selectedValue
      );

    return (
      <View style={[ pickerContainer ]}>
        <TouchableOpacity
          style={[
            touchableStyle,
            this.state.isCollapsed ? {} : touchableStyleActive,
          ]}
          onPress={() => this.toggleCollapsible() }
        >
        { selectedOption &&
          <Text style={this.props.locals.valueStyle} numberOfLines={1}>{selectedOption.NAMA}</Text>
        }
        </TouchableOpacity>
        <Animated.View
          style={{ height: this.state.height, overflow: "hidden" }}
        >
          <Picker
            accessibilityLabel={this.props.locals.label}
            ref="input"
            style={selectStyle}
            selectedValue={this.props.locals.selectedValue}
            onValueChange={this.props.locals.onSelected}
            help={this.props.locals.help}
            enabled={this.props.locals.enabled}
            mode={this.props.locals.mode}
            prompt={this.props.locals.prompt}
            itemStyle={this.props.locals.itemStyle}
          >
            {options}
          </Picker>
        </Animated.View>
      </View>
    );
  }
}

PickerCollapsible.propTypes = {
  locals: PropTypes.object
};

import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Images, Strings } from '../Themes/'
import { Dialog } from 'react-native-simple-dialogs'
import { isNull, isNotNull, isObject, isString } from '../Lib/CheckUtils'

// Styles
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class ErrorDialog extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.isVisible !== nextProps.isVisible) {
      this.setState({
        isVisible: isNotNull(nextProps.isVisible) ? nextProps.isVisible : false
      })
    }
  }

  _toggleDialog() {
    this.setState((prev) => {
      return {isVisible: !prev.isVisible}
    })
  }

  _getTitle() {
    if(this.props.customMessage) {
      if(this.props.customMessage.hasOwnProperty('status')) {
        switch(this.props.customMessage['status']) {
          case 200:
            return '500 - ERROR'
            break
          default:
            return this.props.customMessage['status'] + ' - ' + this.props.customMessage['problem']
            break
        }
      } else {
        return 'ERROR'
      }
    } else {
      return 'xxx - unidentified'
    }
  }

  _getMessage() {
    if(this.props.customMessage) {
      if(this.props.customMessage.hasOwnProperty('data')) {
        let message = this.props.customMessage['data']
        if(isObject(message)) return null
        else if(isString(message)) return message
      }
    }
    return null
  }

  render() {
    let title = this._getTitle()
    let message = this._getMessage()
    return (
      <Dialog
        visible={this.state.isVisible}
        animationType='fade'>
        <View style={{flexDirection: 'column', elevation: 12}}>

          <Image source={this.props.icon} style={{ alignSelf: 'center', width:70, height:70 }} />
          <Text style={[stylesTheme.mediumBold, stylesTheme.greyColor, { textAlign: 'center', marginTop: 10, fontSize: 14} ]}>
            {title}
          </Text>
          { message &&
            <ScrollView style={{height:(deviceHeight/2)-50}}>
              <Text style={[stylesTheme.mediumBold, stylesTheme.greyColor, { textAlign: 'center', marginTop: 10, fontSize: 14} ]}>
                {message}
              </Text>
            </ScrollView>
          }

          <View style={{ marginTop: 30, flexDirection: 'row', }}>
            <TouchableOpacity style={[styles.buttonDialog, {flex:1, alignSelf: 'stretch', backgroundColor: Colors.bgLight}]}
              onPress={this._toggleDialog.bind(this)}>
              <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, {textAlign: 'center', fontSize:14} ]}>
                OK
              </Text>
            </TouchableOpacity>
          </View>

        </View>
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  buttonDialog: {
    flex:1,
    width: 250,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    elevation: 7,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
});

ErrorDialog.propTypes = {
  isVisible: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  onPress: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  customMessage: PropTypes.object,
  icon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

ErrorDialog.defaultProps = { }

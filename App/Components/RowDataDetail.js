import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types'
import { Colors, Metrics } from '../Themes/'
import LinearGradient from 'react-native-linear-gradient';
import stylesTheme from '../Themes/StyleSub'
import { getReadableDate, isValidDate } from '../Lib/DateUtils'

export default class RowDataDetail extends Component {

  constructor(props) {
    super(props);
  }

  getDetail(input) {
    var output;
    if(input.ID_ADT) {
      output = [
        {key: "Role od ADT", value: input.ADT_ROLE},
        {key: "Treatment Detail", value: input.TREATMENT_DETAIL},
        {key: "ADT ID", value: input.ID}
      ];
    } else if(input.ID_ANTIANDROGEN) {
      output = [
        {key: "Antiandrogen Name", value: input.ANTIANDROGEN_NAME},
        {key: "Other Antiandrogen Name", value: input.NAME_OTHER},
        {key: "Antiandrogen Start Date", value: input.START_DATE},
        {key: "Antiandrogen Continuing or Stopped", value: input.CONTINUE_STOP_NAME},
        {key: "Antiandrogen Final Prescription Date", value: input.PRESCRIPTION_FAINAL_DATE},
        {key: "Days of final antiandrogen prescription", value: input.PRESCRIPTION_FINAL_DAYS}
      ];
    } else if(input.ID_COMORBIDITY) {
      output = [
        {key: "Comorbidity", value: input.COMORBIDITY_NAME},
        {key: "Hypertension", value: input.HYPERTENSION_NAME},
        {key: "Heart Disease", value: input.HEART_DISEASE_NAME},
        {key: "Cerebro-vascular disease", value: input.CEREBRO_VASCULAR_NAME},
        {key: "Hyperlipidemia", value: input.HYPERLIPIDEMIA_NAME},
        {key: "Diabetes Melitus", value: input.DIABETES_MELITUS_NAME},
        {key: "Pulmonary Disease", value: input.PULMONARY_DISEASE_NAME},
        {key: "Gastric Ulcer", value: input.GASTRIC_ULCER_NAME},
        {key: "Renal Disease", value: input.RENAL_NAME},
        {key: "Cancer other than Prostate Cancer", value: input.CANCER_OTHER_PROSTATE_NAME},
        {key: "Name of Cancer", value: input.CANCER_NAME},
        {key: "Other Disease", value: input.DISEASE_OTHER},
        {key: "Other Disease Detail", value: input.DISEASE_OTHER_DETAIL}
      ];
    } else if(input.ID_CRPC) {
      output = [
        {key: "Name of CRPC treatment or Chemotherapy", value: input.CRPC_NAME},
        {key: "Other CRPC Treatment Name", value: input.NAME_OTHER},
        {key: "Start Date of this Medication", value: input.START_DATE},
        {key: "This Medication continuing or stopped", value: input.CONTINUE_STOP_NAME},
        {key: "Final Prescription or Injection Date", value: input.PRESCRIPTION_FINAL_DATE},
        {key: "Number of Days of Final Prescription", value: input.PRESCRIPTION_FINAL_DAYS}
      ];
    } else if(input.ID_INITIAL) {
      output = [
        {key: "Treatment", value: input.TREATMENT},
        {key: "Other Treatment Detail", value: input.TREATMENT_OTHER}
      ];
    } else if(input.ID_LHRH) {
      output = [
        {key: "Type and Name", value: input.LHRH_TYPE},
        {key: "Other LHRH a/a name", value: input.NAME_OTHER},
        {key: "Date of 1st LHRH a/a injection or surgical castration", value: input.FIRST_INJECTION_DATE},
        {key: "LHRH a/a continuing or stopped", value: input.CONTINUE_STOP_NAME},
        {key: "LHRH a/a final injection date", value: input.FINAL_INJECTION_DATE},
        {key: "Type of Finally Injected LHRH a/a", value: input.LHRH_INJECTION_TYPE}
      ];
    } else if(input.ID_PROSTATECTOMY) {
      output = [
        {key: "Aim", value: input.PROSTATECTOMY_AIM},
        {key: "Date of Operation", value: input.OPERATION_DATE},
        {key: "Operation Procedure", value: input.OPERATION_PROCEDURE},
        {key: "Other Operation Detail", value: input.OPERATION_OTHER_DETAIL},
        {key: "Lymph Node Dissection", value: input.LYMPH},
        {key: "Nerve Sparing", value: input.NERVE_SPARING}
      ];
    } else if(input.ID_RADIATION) {
      output = [
        {key: "Aim", value: input.RADIATION_AIM},
        {key: "Type", value: input.RADIATION_TYPE},
        {key: "Total Radiation Dosen in Gy", value: input.TOTAL_DOSE_GY},
        {key: "Total Radiation Dose in Gy Detail (SEED EBRT Combination)", value: input.TOTAL_DOSE_GY_DETAIL},
        {key: "RTx Start Date", value: input.RTX_START_DATE},
        {key: "RTx End Date", value: input.RTX_END_DATE}
      ];
    } else if(input.ID_PSA) {
      output = [
        {key: "Latest PSA Range", value: input.PSA_RANGE},
        {key: "Latest PSA Value", value: input.VALUE},
        {key: "Latest PSA Date", value: input.DATE}
      ];
    } else if(input.ID_STATUS) {
      output = [
        {key: "Status", value: input.STATUS},
        {key: "Status Date", value: input.STATUS_DATE},
        {key: "Other Death Detail", value: input.DEATH_OTHER_DETAIL},
        {key: "Other Status Detail", value: input.STATUS_OTHER_DETAIL}
      ];
    } else if(input.ID_SURVEILLANCE) {
      output = [
        {key: "Active Surveillance or Watchful Waiting", value: input.SURVEILLANCE_TYPE},
        {key: "AS Start Date", value: input.START_DATE},
        {key: "AS/WW continuing or stopped", value: input.CONTINUE_STOP_NAME},
        {key: "AS Stopped Date", value: input.STOP_DATE},
        {key: "Reason for stop", value: input.STOP_REASON}
      ];
    } else if(input.ID_OTHER_TREATMENT) {
      output = [
        {key: "Other Treatment Name", value: input.TREATMENT},
        {key: "Name of Other Treatment not listed", value: input.OTHER_NOT_LISTED},
        {key: "Start Date of this Treatment", value: input.START_DATE},
        {key: "This Treatment Continuing or Stopped", value: input.CONTINUE_STOP_NAME},
        {key: "Stopped Date of this treatment", value: input.STOP_DATE}
      ];
    } else if(input.ID_TUMOR_BG) {
      output = [
        {key: "PSA Range at Diagnosis", value: input.PSA_RANGE},
        {key: "PSA Value at Diagnosis", value: input.PSA_VALUE},
        {key: "Testosterone at diagnosis", value: input.TESTOSTERONE},
        {key: "Testosterone Unit", value: input.TESTOSTERONE_UNIT},
        {key: "Specimen Type", value: input.SPECIMEN_TYPE},
        {key: "Diagnosis Date", value: input.DIAGNOSIS_DATE},
        {key: "Histological Type", value: input.HISTOLOGICAL_TYPE},
        {key: "Other Histological Type", value: input.HISTOLOGICAL_TYPE_OTHER},
        {key: "Primary Gleason Score", value: input.GLEASON_PRIMARY},
        {key: "Secondary Gleason Score", value: input.GLEASON_SECONDARY},
        {key: "Tertiary Gleason Score", value: input.GLEASON_TERTIARY},
        {key: "Grade Grouping", value: input.GRADE},
        {key: "No of Positive Biopsy Core", value: input.BIOPSY_POSITIVE_CORE},
        {key: "No of total Biopsy Core", value: input.BIOPSY_TOTAL_CORE},
        {key: "T Category", value: input.CAT_T},
        {key: "N Category", value: input.CAT_N},
        {key: "M Category", value: input.CAT_M},
        {key: "M1c Detail", value: input.M1C_DETAIL},
        {key: "CT for Staging", value: input.STAGING_CT_NAME},
        {key: "MRI for Staging", value: input.STAGING_MRI_NAME},
        {key: "Bone Scan for Staging", value: input.STAGING_BONE_NAME},
        {key: "PET for Staging", value: input.STAGING_PET_NAME},
        {key: "Gene Test", value: input.GENE_TEST},
        {key: "D`Amico Classification", value: input.DAMICO},
        {key: "CAPRA", value: input.CAPRA},
        {key: "J-CAPRA", value: input.JCAPRA}
      ];
    }
    return output;
  }

  render() {
    var detail = this.getDetail(this.props.data);
    return (
      <View style={[ stylesTheme.itemWhiteBox, {marginTop:5} ]}>
        <TouchableOpacity
          underlayColor='transparent'
          style={styles.highlightBox}
          onPress={this.props.onPressRow}>

            <View style={styles.boxMain} >
              <View style={styles.boxLeft}>
                <Text style={[ stylesTheme.regularBold, stylesTheme.blackColor, styles.textMain, {fontSize: 14} ]}>
                  {this.props.data.NAMA_PASIEN}
                </Text>
                <Text style={[ stylesTheme.regularBold, stylesTheme.greyColor, styles.textMain, {fontSize: 12} ]}>
                  {this.props.data.NO_RM}
                </Text>
              </View>
              <View style={styles.boxRight}>
                <Text style={[ stylesTheme.regularBold, stylesTheme.blackColor, styles.textMain, {fontSize: 14} ]}>
                  {this.props.data.NAMA_DOKTER_ORISINAL}
                </Text>
                <Text style={[ stylesTheme.regularBold, stylesTheme.greyColor, styles.textMain, {fontSize: 12}  ]}>
                  {this.props.data.NAMA_RS}
                </Text>
              </View>
            </View>
            <View style={styles.border} />

            { detail && detail.map((obj, i) => (
              <View style={styles.boxDetail} key={i} >
                <View style={styles.boxSmall}>
                  <Text style={[ stylesTheme.regularBold, styles.textSub, {fontSize: 14} ]}>
                    {obj.key || "-"}
                  </Text>
                </View>
                <View style={styles.boxBig}>
                  <Text style={[ stylesTheme.regularBold, styles.textMain, {marginLeft: 10, fontSize: 14} ]}>
                    {isValidDate(obj.value) ? getReadableDate(obj.value) : obj.value || "-"}
                  </Text>
                </View>
              </View>
            )) }


        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxParent: {
    flexDirection: 'column',
    flex: 1
  },
  boxMain: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 15,
    marginRight: 15,
    marginLeft: 15,
  },
  boxDetail: {
    flexDirection: 'row',
    flex: 1,
    marginHorizontal: 15,
    marginVertical: 5,
  },
  boxLeft: {
    flex:1
  },
  boxRight: {
    flex:1,
    flexDirection: 'column',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },
  boxSmall: {
    flex:1
  },
  boxBig: {
    flex:2
  },
  textMain: {
    fontSize: 14,
    opacity: 1
  },
  textSub: {
    fontSize: 14,
    opacity: 0.6
  },
  border: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginHorizontal: 15
  }
});

RowDataDetail.propTypes = {
  onPressRow: PropTypes.func,
  data: PropTypes.object,
};

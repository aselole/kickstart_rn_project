import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Images, Strings } from '../Themes/'
import { Dialog } from 'react-native-simple-dialogs'
import { isNull, isNotNull, isObject, isString } from '../Lib/CheckUtils'

// Styles
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class SuccessDialog extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.isVisible !== nextProps.isVisible) {
      this.setState({
        isVisible: isNotNull(nextProps.isVisible) ? nextProps.isVisible : false
      })
    }
  }

  _toggleDialog() {
    this.setState((prev) => {
      return {isVisible: !prev.isVisible}
    })
    this.props.onPressOk()
  }

  render() {
    return (
      <View>
        <Dialog
          visible={this.state.isVisible}
          onTouchOutside={() => this.setState({ isVisible: true })}
          animationType='fade'>
          <View style={{flexDirection: 'column', elevation: 12}}>

            <Image source={this.props.icon} style={{ alignSelf: 'center', width:70, height:70 }} />
            <Text style={[stylesTheme.mediumBold, stylesTheme.greyColor, { textAlign: 'center', marginTop: 10, fontSize: 14} ]}>
              {this.props.title}
            </Text>
            { this.props.message &&
              <View>
                <Text style={[stylesTheme.mediumBold, stylesTheme.greyColor, { textAlign: 'center', marginTop: 10, fontSize: 14} ]}>
                  {this.props.message}
                </Text>
              </View>
            }

            <View style={{ marginTop: 30, flexDirection: 'row', }}>
              <TouchableOpacity style={[styles.buttonDialog, {flex:1, alignSelf: 'stretch', backgroundColor: Colors.bgLight}]}
                onPress={this._toggleDialog.bind(this)}>
                <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, {textAlign: 'center', fontSize:14} ]}>
                  OK
                </Text>
              </TouchableOpacity>
            </View>

          </View>
        </Dialog>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonDialog: {
    flex:1,
    width: 250,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    elevation: 7,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
});

SuccessDialog.propTypes = {
  isVisible: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  onPress: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onPressOk: PropTypes.func,
};

SuccessDialog.defaultProps = { }

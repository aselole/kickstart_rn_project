import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types'
import StyleSub, {shadowOffsetWidth, shadowOffsetHeight, shadowRadius, shadowOpacity} from '../Themes/StyleSub';
import LinearGradient from 'react-native-linear-gradient';

export default class BoxOverview extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <LinearGradient
        start={{x: 0.2, y: 0.2}} end={{x: 1.0, y: 2.0}}
        colors={['rgb(255, 255, 255)', 'rgb(255, 255, 255)']}
        style={[ styles.boxMain, ]}>
        <TouchableOpacity
          style={styles.highLightBoxMain}
          onPress={this.props.onPress}>
          <View>
            <Image
              source={
                this.props.isFinished ?
                  require('../Assets/ic_checked.png') :
                  require('../Assets/ic_add.png')}
              style={[styles.boxIcon, {width:20, height:20}]}
            />
            <Text style={[
              StyleSub.extraBold,
              StyleSub.itemHeaderText,
              StyleSub.lightgreyColor,
            ]}>
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  boxMain: {
    borderWidth: 0,
    borderRadius: 8,
    marginTop: 15,
    elevation: 12,
    shadowOffset: {
      width: 4,
      height: shadowOffsetHeight
    },
    shadowRadius: 8,
    shadowOpacity: shadowOpacity
  },
  boxLeft: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 0,
  },
  boxRight: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 8,
  },
  highLightBoxMain: {
    height: 135,
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderRadius: 9,
    paddingTop: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  boxIcon: {
    marginBottom: 24,
  },
});

BoxOverview.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  onPress: PropTypes.func,
  isFinished: PropTypes.bool,
};

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import ImageFitLoading from './ImageFitLoading'

export default class ToolbarButton extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={[styles.container, this.props.styleContainer]}>
          <ImageFitLoading urlDefault={this.props.image} style={[styles.icon, this.props.styleIcon]} />
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    flexGrow: 1,
    alignSelf: 'center',
  }
});

ToolbarButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  image: PropTypes.number,
  styleIcon: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
}

ToolbarButton.defaultProps = {

}

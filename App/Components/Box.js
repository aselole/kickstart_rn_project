import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableHighlight, View, Text, Image } from 'react-native'
import styles from './Styles/BoxStyles'
import ExamplesRegistry from '../Services/ExamplesRegistry'

// Note that this file (App/Components/FullButton) needs to be
// imported in your app somewhere, otherwise your component won't be
// compiled and added to the examples dev screen.

// Ignore in coverage report
/* istanbul ignore next */
ExamplesRegistry.addComponentExample('Box', () =>
  <Box
    text='Hey there'
    onPress={() => window.alert('Box Pressed!')}
  />
)

export default class Box extends Component {
  static propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    onPress: PropTypes.func,
    styles: PropTypes.object
  }

  render () {
    return (
      <TouchableHighlight style={[styles.touchable, this.props.styles]} onPress={this.props.onPress}>
        <View>
          <Image style={styles.images}
            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}} />
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.subtitle}>{this.props.subtitle}</Text>
        </View>
      </TouchableHighlight>
    )
  }
}

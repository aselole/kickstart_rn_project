import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Images, Strings } from '../Themes/'
import { Dialog } from 'react-native-simple-dialogs'
import { isNull, isNotNull, isObject, isString } from '../Lib/CheckUtils'
import Text from './Text'
import ImageFitLoading from './ImageFitLoading'
var _ = require('lodash')

// Styles
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class ErrorPopup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: isNotNull(props.isVisible) ? props.isVisible : false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.isVisible !== nextProps.isVisible) {
      this.setState({ isVisible: nextProps.isVisible })
    }
  }

  _onClosePopup() {
    this.setState({ isVisible: false })
  }

  render() {
    let isVisible = isNull(this.state.isVisible) ? false : this.state.isVisible
    return (
      <View style={[ {flex: this.state.isVisible ? 1 : 0, position:'absolute', bottom:20, left:10, right:10}, this.props.styleContainer ]}>
        { this.state.isVisible &&
          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderRadius: 20,
            backgroundColor: 'white',
            padding:10,
            elevation: 4,
            shadowOffset: {
              width: 2,
              height: 2,
            },
            shadowRadius: 4,
            shadowOpacity: 0.5,
            minHeight: 35,
          }}>
            <View style={{ flex:0.1, alignItems:'center', justifyContent: 'center', backgroundColor:'transparent' }}>
              <ImageFitLoading urlDefault={this.props.icon} style={{ flexGrow:1 }} />
            </View>
            <Text numberOfLines={2} style={{ flex:1, color:Colors.bgDark, backgroundColor:'transparent', padding:5 }}>{_.trimStart(this.props.message)}</Text>
            <View style={{ flex:0.1, alignItems:'center', justifyContent: 'center', backgroundColor:'transparent' }}>
              <TouchableOpacity onPress={() => this._onClosePopup()}>
                <ImageFitLoading urlDefault={Images.cancel} style={{ flexGrow:1 }} />
              </TouchableOpacity>
            </View>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonDialog: {
    flex:1,
    width: 250,
    height: 40,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    elevation: 7,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
});

ErrorPopup.propTypes = {
  isVisible: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  message: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  styleContainer: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

ErrorPopup.defaultProps = {
  isVisible: false,
  icon: Images.info
}

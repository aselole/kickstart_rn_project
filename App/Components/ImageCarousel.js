import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import FastImage from 'react-native-fast-image'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import ImageLoading from './ImageLoading'
import { updateArray, initWithBool, deleteArray } from '../Lib/ImmutableUtils'
import { stringToArray } from '../Lib/ManipulationUtils'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'

export default class ImageCarousel extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentImageIndex: 0,
      data: null,
    }
  }

  componentWillMount() {
    let images = Array.isArray(this.props.data) ?                   //ubah url image ke format standar untuk ImageLoading yaitu array
                  this.props.data :
                  stringToArray(this.props.data)
    images = (images == null || typeof images === 'undefined') ?    //jika image kosong maka tampilkan template image kosong dari server
              ["no_image.png"] :
              images
    this.setState({ data: images })
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.data !== nextProps.data) {
      if(nextProps.data !== null) {
        this.setState({ data: nextProps.data })
      }
    }
  }

  _onHandleIndexChanged(index) {
    this.setState({ currentImageIndex: index })
  }

  _renderItem({item, index}) {
    return (
       <View style={styles.sliderContent}>
         <ImageLoading
           style={[styles.imageContent]}
           name={item}
           path={item}
           urlDefault={this.props.remoteDefaultUrl}
           urlBase={this.props.remoteBaseUrl} />
       </View>
    );
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        { this.state.data &&
          <View style={styles.subContainer}>
            <Carousel
               ref={(c) => { this._carousel = c; }}
               data={this.state.data}
               firstItem={0}
               enableSnap={true}
               renderItem={this._renderItem.bind(this)}
               sliderWidth={sliderWidth}
               itemWidth={itemWidth}
               onSnapToItem={(index) => this._onHandleIndexChanged(index) }
               containerCustomStyle={styles.sliderContainer}
               contentContainerCustomStyle={styles.sliderContentContainer}
            />
            <Pagination
              dotsLength={this.state.data.length}
              activeDotIndex={this.state.currentImageIndex}
              inactiveDotOpacity={0.7}
              inactiveDotScale={0.55}
              containerStyle={styles.pagination}
            />
          </View>
         }
      </View>
    )
  }
}

const horizontalMargin = 20
const sliderHeight = 250
const sliderWidth = deviceWidth
const itemWidth = sliderWidth - (horizontalMargin * 3)
const itemHeight = 250

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: sliderHeight,
  },
  subContainer: {
    flex: 1,
  },
  sliderContainer: {
    marginBottom: 0
  },
  sliderContentContainer: {
    marginBottom: 0
  },
  sliderContent: {
    width: itemWidth,
    height: itemHeight,
  },
  imageContent: {
    position: 'relative',
    flex:1
  },
  deleteContainer: {
    position: 'absolute',
    top: 5,
    right: 5,
    overflow:'visible'
  },
  deleteIcon: {
    width:20,
    height:20
  },
  pagination: {
    paddingTop: 10,
    paddingBottom: 5,
  }
})

ImageCarousel.propTypes = {
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string ]),
  isSourceLocal: PropTypes.bool,
  remoteDefaultUrl: PropTypes.string,
  remoteBaseUrl: PropTypes.string
}

ImageCarousel.defaultProps = {
  isSourceLocal: false,
}

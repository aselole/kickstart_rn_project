import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text, Image } from 'react-native'
import styles from './Styles/BoxStyles'
import ExamplesRegistry from '../Services/ExamplesRegistry'

// Note that this file (App/Components/FullButton) needs to be
// imported in your app somewhere, otherwise your component won't be
// compiled and added to the examples dev screen.

// Ignore in coverage report
/* istanbul ignore next */
ExamplesRegistry.addComponentExample('BoxStatus', () =>
  <Box
    text='Hey there'
    onPress={() => window.alert('BoxStatus Pressed!')}
  />
)

export default class BoxStatus extends Component {
  static propTypes = {
    title: PropTypes.string,
    isFinished: PropTypes.bool,
    onPress: PropTypes.func,
    styles: PropTypes.object
  }

  static defaultProps = {
    isFinished: false
  };

  render () {
    var icon = this.props.isFinished
                ? require('../Assets/ic_checked.png')
                : require('../Assets/ic_add.png');
    return (
      <TouchableOpacity style={[styles.touchable, this.props.styles]} onPress={this.props.onPress}>
        <View>
          <Image style={styles.images} source={icon} />
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import { Strings, Images } from '../Themes/'
import UserActions from '../Redux/UserRedux'
import { MenuButton, ConfirmDialog } from '../Components'

class DrawerContainer extends Component {

  _logout = () => {
    this.props.navigation.navigate({ routeName: 'LoginScreen' })
    this.props.logout()
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={styles.container}>
        <Text style={[styles.title, {marginTop: 0}]}>{Strings.existing}</Text>
        <MenuButton
          onPress={() => navigation.navigate('ExistingNewScreen')}
          image={Images.add}
          title={Strings.menu_existing_baru}>
        </MenuButton>
        <MenuButton
          onPress={() => navigation.navigate('ExistingListScreen')}
          image={Images.map}
          title={Strings.menu_peta_exist}>
        </MenuButton>
        <MenuButton
          onPress={() => navigation.navigate('ExistingScreen')}
          image={Images.list}
          title={Strings.menu_rekap_exist}>
        </MenuButton>

        <Text style={styles.title}>{Strings.usulan}</Text>
        <MenuButton
          onPress={() => navigation.navigate('UsulanNewScreen')}
          image={Images.add}
          title={Strings.menu_usulan_baru}>
        </MenuButton>
        <MenuButton
          onPress={() => navigation.navigate('UsulanListScreen')}
          image={Images.map}
          title={Strings.menu_peta_usulan}>
        </MenuButton>
        <MenuButton
          onPress={() => navigation.navigate('UsulanScreen')}
          image={Images.list}
          title={Strings.menu_rekap_usulan}>
        </MenuButton>

        <Text style={styles.title}>{Strings.akun}</Text>
        <MenuButton
          onPress={this._logout}
          image={Images.logout_black}
          title={Strings.logout}
          isNeedConfirm={true}
          confirmTitle='Logout'
          confirmMessage='Apakah Anda yakin ingin keluar dari sesi?'
          confirmIcon={Images.logout_bold}>
        </MenuButton>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
    paddingTop: 40,
    paddingHorizontal: 20
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10
  }
})

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(UserActions.logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContainer)

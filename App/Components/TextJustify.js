import React, { Component } from "react"
import { View, StyleSheet, Dimensions, WebView}  from "react-native"
import PropTypes from 'prop-types'
import MyWebView from 'react-native-webview-autoheight'
var _ = require('lodash')

const { width, height, scale } = Dimensions.get("window")

export default class TextJustify extends Component {

  constructor(props) {
    super(props);
    this.state = {
      html: null,
      style: "<style>"+
                "*{text-align:justify; line-height: 1.8; font-size: 14; letter-spacing: 0.5px; font-family: Helvetica;}"+
             "</style>"
    }
  }

  componentDidMount() {
    this._setHTML(this.props.text)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.text, nextProps.text) && !_.isNull(nextProps.text)) {
      this._setHTML(nextProps.text)
    }
  }

  // isi state dengan html generate-an
  _setHTML(input) {
    this.setState({ html: this._generateHTML(input) })
  }

  // enkapsulasi konten html dengan tag html
  _generateHTML(input) {
    if(this.props.isStyleClear) {
      input = _.replace(input, new RegExp('style="[^"]*"','g'), '')   // hapus style yg kebawa html, pakai style yg ditentukan state
    }
    return '<!DOCTYPE html><html><body><script>window.location.hash = 1;document.title = document.height;</script>'+input+'</body></html>'
  }

  onNavigationStateChange(navState) {
    this.setState({
      height: navState.title,
    });
  }

  render() {
    if(this.state.html) {
      return (
        <MyWebView
          source={{html: this.state.style + this.state.html}}
          width={width-16}    // lebar container webview
          startInLoadingState={true}
          style={[ styles.container, this.props.styleContainer ]}
        />
      )
    } else {
      return null
    }
  }

}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center'
  },
})

TextJustify.propTypes = {
  text: PropTypes.string,   // konten html bisa berupa teks biasa atau pakai tag html
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),  // style buat container webview
  isStyleClear: PropTypes.bool,   // konten html yg kadung diatur style-nya, di non-aktifkan atau tidak stylenya
};

TextJustify.defaultProps = {
  isStyleClear: true
};

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types'
import { Colors, Metrics } from '../Themes/'
import LinearGradient from 'react-native-linear-gradient';
import stylesTheme from '../Themes/StyleSub'
import { getReadableDate, isValidDate } from '../Lib/DateUtils'

export default class DetailRow extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let borderWidth = this.props.isBordered ? 0.5 : 0;
    return (
      <View>
        <View style={[ styles.boxDetail, {borderBottomWidth: borderWidth} ]}>
          <View style={styles.boxSmall}>
            <Text style={[ stylesTheme.regularBold, styles.textSub ]}>
              {this.props.title}
            </Text>
          </View>
          <View style={styles.boxBig}>
            <Text style={[ stylesTheme.regularBold, styles.textMain, {alignSelf: 'flex-end'} ]}>
              {this.props.value || '-'}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxDetail: {
    flexDirection: 'row',
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: '#c9c9c9',
  },
  boxSmall: {
    flex:1
  },
  boxBig: {
    flex:2
  },
  textMain: {
    fontSize: 14,
    opacity: 1
  },
  textSub: {
    fontSize: 14,
    opacity: 0.6
  },
  border: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginTop: 5,
  }
});

DetailRow.propTypes = {
  title: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  isBordered: PropTypes.bool,
  isCorner: PropTypes.bool
};

DetailRow.defaultProps = {
  isBordered: true,
  isCorner: false,
};

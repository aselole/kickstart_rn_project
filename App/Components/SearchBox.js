import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'
import Icon from 'react-native-vector-icons/FontAwesome'
import CommonStyles, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub';

export default class SearchBox extends Component {
  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onChangeTerm: PropTypes.func.isRequired,
    searchTerm: PropTypes.string,
    imgLeft: PropTypes.func,
    imgRight: PropTypes.func
  }

  render () {
    const { onSearch, onCancel, onChangeTerm, searchTerm } = this.props
    const onSubmitEditing = () => onSearch(searchTerm)
    return (
      <View style={[ stylesTheme.itemWhiteBox, styles.mainBox ]}>
        <View style={styles.leftCol}>
          <View style={styles.iconBox}>
            <Image source={require('../Assets/icSearch.png')} style={{width:30, height:30}} />
          </View>
          <View style={{
            width: 0.7,
            height: 25,
            backgroundColor: 'rgb(229,229,229)'}}
          />
        </View>
        <View style={styles.centerText}>
          <TextInput
            ref='searchText'
            autoFocus
            placeholder='Nama Pasien/No. Rekam Medik'
            underlineColorAndroid='transparent'
            style={styles.searchInput}
            value={this.props.searchTerm}
            onChangeText={onChangeTerm}
            onSubmitEditing={onSubmitEditing}
            autoCapitalize='none'
            returnKeyType={'search'}
            autoCorrect={false}
            placeholderTextColor={Colors.themeLight}
            selectionColor={Colors.themeLight}
            textColor={Colors.themeLight}
          />
        </View>
        <TouchableOpacity style={styles.rightCol} onPress={onCancel}>
          <Image source={require('../Assets/icClear.png')} style={{width:30, height:30}} />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: Platform.OS === 'ios' ? 8 : 0,
    paddingBottom: Platform.OS === 'ios' ? 8 : 0,
    marginBottom: 10,
  },
  leftCol: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 45,
  },
  iconBox: {
    width: 37,
  },
  centerText: {
    flex: 1,
  },
  rightCol: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInput: {
    fontSize: 15,
    lineHeight: 12
  }
});

import React, { Component } from 'react'
import { Platform, StyleSheet, Image, Animated, View, TouchableOpacity, Text, Picker } from 'react-native'
import PropTypes from 'prop-types'
import styles from './Styles/BoxStyles'
import stylesTheme from '../Themes/StyleSub'
import Icon from 'react-native-vector-icons/FontAwesome'
import PickerCollapsible from './PickerCollapsible'

export default class BoxPickerCollapsible extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapseParent: true,
    };
  }

  getConfig() {
    return {
      title: this.props.title,
      options: this.props.data,
      selectedValue: this.props.selectedValue,
      label: this.props.prompt,
      onSelected: this.props.onSelected,
      help: this.props.help,
      enabled: this.props.enabled,
      mode: this.props.mode,
      prompt: this.props.prompt,
      isCollapseParent: this.state.isCollapseParent,
      itemStyle: this.props.itemStyle,
      valueStyle: this.props.valueStyle,
      titleStyle: this.props.titleStyle
    };
  }

  render() {
    let formGroupStyle = styles.formGroupNormal;
    let controlLabelStyle = styles.controlLabelNormal;
    let selectStyle = styles.selectNormal;
    let helpBlockStyle = styles.helpBlockNormal;
    let errorBlockStyle = styles.errorBlock;

    const label =
      this.props.prompt ? ( <Text style={this.props.titleStyle}>{this.props.prompt}</Text> ) : null;
    const help =
      this.props.help ? ( <Text style={helpBlockStyle}>{this.props.help}</Text> ) : null;

    if(!this.props.data || this.props.data.length == 0) {
      return null;
    }

    var options = this.props.data.map((item, key) => (
      <Picker.Item key={item.ID} value={item.ID} label={item.NAMA} />
    ));

    return (
      <View style={[ stylesTheme.itemWhiteBox ]} >
        <View style={styles.detailItemBox}>

          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={this.props.imgLeft} style={{ marginTop: 6, width:30, height:30}} />
            </View>
            <View style={{ marginTop: 9, width: 0.7, height: 25, backgroundColor: 'rgb(229,229,229)'}} />
          </View>

          <TouchableOpacity style={styles.centerInfo}
            onPress={() => { this.setState({ isCollapseParent: !this.state.isCollapseParent }) }}>
            <View style={formGroupStyle}>
              {label}
                <PickerCollapsible locals={this.getConfig()} />
              {help}
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={[ styles.rightCol, {marginTop: 13} ]}
            onPress={() => { this.setState({ isCollapseParent: !this.state.isCollapseParent }) }}>
            <Image
              source={this.props.imgRight}
              style={{height: 20, width: null}} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

BoxPickerCollapsible.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  selectedValue: PropTypes.oneOfType([PropTypes.array, PropTypes.number, PropTypes.object, PropTypes.string]),
  onSelected: PropTypes.func,
  help: PropTypes.string,
  enabled: PropTypes.bool,
  mode: PropTypes.string,
  prompt: PropTypes.string,
  itemStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  valueStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
};

BoxPickerCollapsible.defaultProps = {
  itemStyle: [ stylesTheme.light, stylesTheme.mediumText ],
  valueStyle: [ stylesTheme.light, stylesTheme.smallText, {lineHeight:0, alignSelf: 'flex-start', backgroundColor: 'transparent'} ],
  titleStyle: [ stylesTheme.light, stylesTheme.smallText, stylesTheme.lightgreyColor, {lineHeight:0} ],
}

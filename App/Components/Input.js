import React, {Component} from "react"
import {Text, View, TextInput, Image} from "react-native"

import css from "../Themes/Style"

export default class Input extends Component {
    render() {
        return (
            <View style={css.buttonRound}>
              <TextInput
                  underlineColorAndroid='rgba(0,0,0,0)'
                  style={css.inputDark}
                  placeholderTextColor={"#aaa"}
                  {...this.props} />
            </View>
        );
    }

}

import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, Platform, TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import LinearGradient from 'react-native-linear-gradient'
import { Colors, Images, Servers, Strings } from '../Themes/'
import css from '../Themes/Style'

export default class MenuItemBox extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        style={this.props.styleContainer}
        onPress={this.props.onPress}>
        <Image
          style={this.props.styleImage}
          source={this.props.image} />
        <Text style={this.props.styleTitle}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({

});

MenuItemBox.propTypes = {
  onPress: PropTypes.func,
  image: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  title: PropTypes.string,
  styleContainer: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleImage: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleTitle: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
};

MenuItemBox.defaultProps = {
  styleContainer: css.templateRow,
  styleImage: css.templateImage,
  styleTitle: css.templateMenu,
}

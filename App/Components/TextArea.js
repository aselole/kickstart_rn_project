import React, { Component } from "react"
import {
  Dimensions, View, TextInput, Image,
  StyleSheet
} from "react-native"
import PropTypes from 'prop-types'
import Textarea from 'react-native-textarea'
import Text from './Text'
import { Fonts } from '../Themes/'
var _ = require('lodash')

import css from "../Themes/Style"
const { width, height, scale } = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

export default class TextArea extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error, nextProps.error)) {
      this.setState({ error: nextProps.error })
    }
  }

  _onChangeText(text) {
    this.setState({ error: null })
    if(this.props.hasOwnProperty('onChange')) {
      this.props.onChange(text)
    }
  }

  render() {
    return (
      <View style={[ styles.textareaViewContainer, this.props.styleContainer ]}>
        <Textarea
          containerStyle={[ styles.textareaContainer, this.props.styleInputContainer ]}
          style={[ styles.textarea, this.props.styleInput ]}
          defaultValue={this.props.text}
          maxLength={this.props.maxLength}
          placeholder={this.props.title}
          placeholderTextColor={this.props.placeholderColor}
          underlineColorAndroid={'transparent'}
          {...this.props}
        />
      </View>
    )
  }

}

const styles = StyleSheet.create({
  textareaViewContainer: {
    height: vh * 50,
    borderColor: "#ddd",
    borderWidth: 1,
    fontSize: 14,
    borderRadius: 4,
    padding: 4,
    paddingLeft: 8,
    marginHorizontal: 20,
    marginBottom: 8,
    color: '#333',
    backgroundColor:'transparent'
  },
  textareaContainer: {
    height: vh * 50,
  },
  textarea: {
    textAlignVertical: 'top',  // hack android
    height: vh * 49,
    fontFamily: Fonts.type.base,
    fontSize: 14,
  },
})

TextArea.propTypes = {
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleInputContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleInput: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  title: PropTypes.string,
  text: PropTypes.string,
  maxLength: PropTypes.number,
  placeholderColor: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
};

TextArea.defaultProps = {};

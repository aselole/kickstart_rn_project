import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'
import { getFirstParam } from '../Lib/UrlUtils'
import { isLocalImage, isAssetImage } from '../Lib/CheckUtils'
import { convertError } from '../Lib/AppCustomUtils'
import LoadingOverlay from './LoadingOverlay'
import NotifDialog from './NotifDialog'
var _ = require('lodash')

export default class ModalLoading extends Component {

  constructor(props) {
    super(props)
    this.state = {
      msg_error: null,
      msg_ok: null,
      is_fetching: false,
      msg_loading: ''
    }
  }

  componentDidMount() {
    this.setState({ msg_loading: this.props.title })
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.title, nextProps.title)) {
      this.setState({ msg_loading: nextProps.title })
    }
    if(!_.isEqual(this.props.responseFail, nextProps.responseFail) && !_.isNull(nextProps.responseFail)) {
      this.setState({
        msg_error: convertError(nextProps.responseFail),
        msg_ok: null,
      })
    }
    if(!_.isEqual(this.props.responseOk, nextProps.responseOk) && !_.isNull(nextProps.responseOk)) {
      this.setState({
        msg_ok: convertError(nextProps.responseOk),
        msg_error: null,
      })
    }
    if(!_.isEqual(this.props.isFetching, nextProps.isFetching) && !_.isNull(nextProps.isFetching)) {
      this.setState({ is_fetching: nextProps.isFetching })
    }
  }

  _onLoadingFinished() {
    if(!this.props.isShowConfirmation) return
    if(!_.isNull(this.state.msg_error)) {
      this.setState({ notif_error_visible: true })
    }
    if(!_.isNull(this.state.msg_ok)) {
      this.setState({ notif_ok_visible: true })
    }
    this.props.onLoadingFinished()
  }

  _onNotifErrorConfirm() {
    this.setState({ notif_error_visible: false }, () => {
      this.props.onConfirmResponseFail()
    })
  }

  _onNotifOkConfirm() {
    this.setState({ notif_ok_visible: false }, () => {
      this.props.onConfirmResponseOk()
    })
  }

  render () {
    return (
      <View>
        { this.props.isVisible &&
          <View>
            <LoadingOverlay
              title={this.state.msg_loading}
              isLoading={this.state.is_fetching}
              isIndeterminate={true}
              customIndeterminateLoading='wave'
              titleStyle={{color:'gray', fontSize:18}}
              onModalHide={() => this._onLoadingFinished()}
            />
          </View>
        }
        { this.props.isVisibleOk &&
          <View>
            <NotifDialog
              isVisible={this.state.notif_ok_visible}
              onPressOk={() => this._onNotifOkConfirm()}
              message={this.state.msg_ok}
              isSuccess={true}
            />
          </View>
        }
        <NotifDialog
          isVisible={this.state.notif_error_visible}
          onPressOk={() => this._onNotifErrorConfirm()}
          message={this.state.msg_error}
          isSuccess={false}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({ });

ModalLoading.propTypes = {
  title: PropTypes.string,
  isVisible: PropTypes.bool,
  isVisibleOk: PropTypes.bool,
  isFetching: PropTypes.bool,
  responseFail: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.string ]),
  responseOk: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.string ]),
  onConfirmResponseOk: PropTypes.func,
  onConfirmResponseFail: PropTypes.func,
  isShowConfirmation: PropTypes.bool
}

ModalLoading.defaultProps = {
  isDisable: false,
  isVisible: true,
  isVisibleOk: true,
  onConfirmResponseOk: () => null,
  onConfirmResponseFail: () => null,
  onLoadingFinished: () => null,
  isShowConfirmation: true,
}

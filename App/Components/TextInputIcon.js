import React, {Component} from "react"
import {Text, View, TextInput, Image, StyleSheet} from "react-native"
import PropTypes from 'prop-types'
var _ = require('lodash')

import css from "../Themes/Style"

export default class TextInputIcon extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error, nextProps.error)) {
      this.setState({ error: nextProps.error })
    }
  }

  _onChangeText(text) {
    this.setState({ error: null })
    if(this.props.hasOwnProperty('onChange')) {
      this.props.onChange(text)
    }
  }

  render() {
    return (
      <View style={[css.buttonRound, this.props.styleContainer ]}>
        <Image source={this.props.image} style={css.inputIcon}></Image>
        <TextInput
            underlineColorAndroid='rgba(0,0,0,0)'
            style={css.textInputDark}
            placeholderTextColor={"#aaa"}
            onChangeText={(text) => this._onChangeText(text)}
            ref={(input) => this.props.inputRef && this.props.inputRef(input)}
            {...this.props} />
        { !_.isEmpty(this.state.error) &&
          <Text style={[this.props.styleError, styles.error]}>
            {this.state.error}
          </Text>
        }
      </View>
    )
  }

}

const styles = StyleSheet.create({
  error: {
    fontSize: 12,
    color:'red',
  },
})

TextInputIcon.propTypes = {
  image: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleError: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  error: PropTypes.string,
  onChange: PropTypes.func,
};

TextInputIcon.defaultProps = {};

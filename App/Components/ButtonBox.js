import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class ButtonBox extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    const { onPress, title, imgLeft, imgRight } = this.props
    return (
      <TouchableOpacity style={[ stylesTheme.itemWhiteBox ]} onPress={onPress}>
        <View style={styles.detailItemBox}>
          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={imgLeft} style={{width:30, height:30}} />
            </View>
            <View style={{
              width: 0.7,
              height: 25,
              backgroundColor: 'rgb(229,229,229)'}}
            />
          </View>
          <View style={styles.centerInfo}>
            <Text style={[
              StyleSub.regularBold,
              StyleSub.normalText,
              StyleSub.greyColor,
            ]}>
            {title}
            </Text>
          </View>
          <TouchableOpacity style={styles.rightCol} onPress={onPress}>
            <Image source={imgRight} style={{width:30, height:30}} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 17,
    paddingHorizontal: 19
  },
  highlightBox: {
    borderRadius: 12,
  },
  leftCol: {
    flexDirection: 'row',
    flex: 1.5,
    alignItems: 'center',
  },
  centerInfo: {
    flex: 5,
  },
  rightCol: {
    flex: 0.5,
    alignItems: 'center',
  },
  icon: {
    width: 41,
  },
});

ButtonBox.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string,
  imgLeft: PropTypes.number,
  imgRight: PropTypes.number
}

ButtonBox.defaultProps = {
  
}

import { Platform, StyleSheet } from 'react-native'
import { Fonts, Metrics, Colors } from '../../Themes/'

var LABEL_COLOR = "#000000";
var INPUT_COLOR = "#000000";
var ERROR_COLOR = "#a94442";
var HELP_COLOR = "#999999";
var BORDER_COLOR = "#cccccc";
var DISABLED_COLOR = "#777777";
var DISABLED_BACKGROUND_COLOR = "#eeeeee";
var FONT_SIZE = 17;
var FONT_WEIGHT = "500";

export default StyleSheet.create({
  touchable: {
    borderColor: Colors.primary,
    borderWidth: 1,
    backgroundColor: Colors.snow,
    borderRadius: 5,
    height: Metrics.doubleBaseSize,
    flex:1,
    height: 120,
  },
  title: {
    margin: 10,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.medium
  },
  subtitle: {
    margin: Metrics.smallMargin,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.bold
  },
  images: {
    width: Metrics.smallImage,
    height: Metrics.smallImage,
    alignSelf: 'center',
    marginTop: Metrics.baseMargin
  },
  buttons: {
    margin: Metrics.baseMargin,
    backgroundColor: Colors.primary,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonTitle: {
    margin: 10,
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.medium
  },

  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 19,
    paddingVertical: 14
  },
  highlightBox: {
    borderRadius: 12,
  },
  leftCol: {
    flexDirection: 'row',
    flex: 1.5,
    alignSelf: 'flex-start'
  },
  centerInfo: {
    flex: 5,
  },
  rightCol: {
    flex: 0.5,
    alignSelf: 'flex-start'
  },
  icon: {
    width: 41,
    alignSelf: 'flex-start'
  },
  pickerContainerNormal: {
    marginBottom: 1,
    borderRadius: 4,
    borderColor: BORDER_COLOR,
    borderWidth: 0
  },
  pickerContainerError: {
    marginBottom: 4,
    borderRadius: 4,
    borderColor: ERROR_COLOR,
    borderWidth: 0
  },
  pickerContainerOpen: {

  },
  selectNormal: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR
      },
      ios: {}
    })
  },
  selectError: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR
      },
      ios: {}
    })
  },
  pickerTouchableNormal: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableError: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableActive: {
    borderBottomWidth: 1,
    borderColor: BORDER_COLOR
  },
  pickerValueNormal: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  pickerValueError: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  formGroupNormal: {
    marginBottom: 0
  },
  formGroupError: {
    marginBottom: 10
  },
  controlLabelNormal: {
    color: LABEL_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  controlLabelError: {
    color: ERROR_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  helpBlockNormal: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  helpBlockError: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: ERROR_COLOR
  },
})

import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Images, Strings } from '../Themes/'
import { Dialog } from 'react-native-simple-dialogs'
import { isNull, isNotNull, isObject, isString } from '../Lib/CheckUtils'

// Styles
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class ConfirmDialog extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.isVisible !== nextProps.isVisible) {
      this.setState({
        isVisible: true
      })
    }
  }

  _onToggleModal() {
    this.setState({
      isVisible: false
    })
  }

  _onPressOk() {
    this._onToggleModal()
    if(this.props.hasOwnProperty('onPressOk')) {
      this.props.onPressOk()
    }
  }

  _onPressCancel() {
    this._onToggleModal()
    if(this.props.hasOwnProperty('onPressCancel')) {
      this.props.onPressCancel()
    }
  }

  render() {
    return (
      <Dialog
        visible={this.state.isVisible}
        animationType='fade'>
        <View style={styles.container}>

          <View style={styles.icon}>
            <Image source={this.props.icon} style={styles.iconImage} />
          </View>

          <Text style={[stylesTheme.semiBold, stylesTheme.greyColor, styles.title ]}>
            {this.props.title}
          </Text>
          <Text style={[stylesTheme.mediumBold, stylesTheme.greyColor, styles.message ]}>
            {this.props.message}
          </Text>

          <View style={{ marginTop: 20, flexDirection: 'row', }}>
            <TouchableOpacity style={[styles.buttonDialog, this.props.styleButtonOk]}
              onPress={this._onPressOk.bind(this)}>
              <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, styles.buttonTitle ]}>
                {this.props.titleOk}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonDialog, this.props.styleButtonCancel]}
              onPress={this._onPressCancel.bind(this)}>
              <Text style={[stylesTheme.mediumBold, stylesTheme.whiteColor, styles.buttonTitle ]}>
                {this.props.titleCancel}
              </Text>
            </TouchableOpacity>
          </View>

        </View>
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    elevation: 12,
  },
  icon: {
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'center',
    width:70,
    height:70,
    borderRadius:35,
    overflow:'hidden',
    backgroundColor:'gray'
  },
  iconImage: {
    alignSelf:'center',
    width:45, height:45
  },
  title: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 14,
  },
  message: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 12,
  },
  buttonDialog: {
    flex:1,
    height: 40,
    backgroundColor: 'red',
    borderRadius: 20,
    elevation: 7,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  buttonTitle: {
    textAlign: 'center',
    fontSize:14,
  }
});

ConfirmDialog.propTypes = {
  isVisible: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  onPressOk: PropTypes.func,
  onPressCancel: PropTypes.func,
  titleOk: PropTypes.string,
  titleCancel: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  styleButtonOk: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleButtonCancel: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
};

ConfirmDialog.defaultProps = {
  titleOk: 'OK',
  titleCancel: 'BATAL',
}

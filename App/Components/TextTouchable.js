import React, {Component} from "react"
import {Text, View, TouchableHighlight} from "react-native"
import {Fonts} from '../Themes'

import css from "../Themes/Style"

export default class IconInput extends Component {
    render() {
        return (
            <TouchableHighlight {...this.props}>
              <Text style={[this.props.textStyle, {fontFamily: Fonts.type.base}]}>{this.props.title}</Text>
            </TouchableHighlight>
        );
    }

}

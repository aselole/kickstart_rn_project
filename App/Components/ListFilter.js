import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, Dimensions, StyleSheet, View, Text,
  Image, SectionList, ScrollView, TouchableOpacity
} from 'react-native'
import { Colors, Strings, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import stylesTheme from '../Themes/StyleSub'
import { DotIndicator, WaveIndicator, PacmanIndicator } from 'react-native-indicators'
import update from 'immutability-helper'
import { filterData } from '../Lib/FilterUtils'
import NotifDialog from './NotifDialog'
import ImageFitLoading from './ImageFitLoading'
import FlexImage from 'react-native-flex-image'
import CollapseView from 'react-native-collapse-view'
import Parallax from '../Lib/react-native-parallax'
import AppConfig from '../Config/AppConfig'
var _ = require('lodash')
var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

/*
 * List data dengan fitur filter
 * metode filter: otomatis filter ketika ada perubahan kata kunci pencarian/parameter)
*/
export default class ListFilter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: null,
      is_empty: false,
      error_title: null,
      collapse_error: false,
    }
  }

  componentDidMount() {
    if(!_.isNil(this.props.data)) {
      this.setState({
        items: this._normalizeData(this.props.data)
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.errorTitle, nextProps.errorTitle) && !_.isNull(nextProps.errorTitle)) {
      this.setState({ error_title: nextProps.errorTitle })
    }
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this._populateList(nextProps.data)
    }
    if(!_.isEqual(this.props.filterParam, nextProps.filterParam) && !_.isNull(nextProps.filterParam)) {
      this._performFilter(nextProps.filterParam)
    }
  }

  // ada update data baru
  _populateList(data) {
    if(_.isArray(data) && !_.isEmpty(data)) {
      this.setState({
        items: this._normalizeData(data)
      }, () => {
        if(this.props.isScrollOnRefresh && this._list) {  // fitur refresh data, terus arahkan view ke data baru
          let wait = new Promise((resolve) => setTimeout(resolve, 500));  // Smaller number should work
          wait.then( () => {
            this._list.scrollToLocation({
              itemIndex: this.state.items[0]['data'][this.state.items[0]['data'].length-1].id,
            })
          });
        }
      })
    } else {
      this.setState({
        is_empty: true,
        error_title: 'Data Kosong'
      })
    }
  }

  // benerin struktur data source yang akan dipakai sectionlist atau parallax
  _normalizeData(data) {
    if(this.props.type === 'section') {
      if(!this.props.isMultiSection) return [{ data: data }]
      else return data
    } else {
      return data
    }
  }

  // pencarian data
  _performFilter(filters) {
    this.setState({
      items: this._normalizeData(this.props.data)
    }, () => {
      let newData =
      ( this.state.items !== null &&
          this.state.items[0].data.filter((item) =>
            filters.every((filter) => {
              if(filter.type === 'dropdown' && filter.value == 'Semua') {         // utk parameter tipe dropdown, jika pilih 'semua' maka tdk perlu filter
                return true
              } else {                                                            // utk parameter tipe lain, periksa apa cocok dgn current data atau tidak
                return item[filter.column].toLowerCase().includes(filter.value.toLowerCase())
              }
            }
        ))
      )
      this.setState(prevState => ({
        items: update(prevState.items, {0: {data: {$set: newData}}})
      }))
    })
  }

  _onParallaxClicked(item) {
    this.props.onItemClicked(item)
  }

  render () {
    if(!_.isNull(this.state.items) && !this.props.isFetching && this.props.type === 'section') {
      return (
        <SectionList
          ref={(ref) => this._list = ref}
          sections={this.state.items}
          renderItem={this.props.renderRow}
          renderSectionHeader={this.props.renderHeader}
          keyExtractor={(item, index) => _.isNull(this.props.keys) ? item.id : item[this.props.keys] }
          horizontal={this.props.isHorizontal}
          style={this.props.style}
          contentContainerStyle={this.props.styleContent}
          showsVerticalScrollIndicator={this.props.isShowScroll}
        />
      )
    } else if(!_.isNull(this.state.items) && !this.props.isFetching && this.props.type === 'parallax') {
      return (
        <Parallax.ScrollView style={{flex:1}}>
          {
            this.state.items.map((item, index) => (
              <Parallax.Image
               key={index}
               style={styles.parallaxImage}
               onPress={() => this._onParallaxClicked(item)}
               overlayStyle={styles.parallaxOverlay}
               source={{ uri: this.props.parallaxBaseImageUri + item[this.props.parallaxImageKey]}}
               parallaxFactor={0.8}>
                {
                  !_.isNil(this.props.parallaxTitleKey) &&
                    <Text style={styles.parallaxTitle}>{item[this.props.parallaxTitleKey]}</Text>
                }
                {
                  !_.isNil(this.props.parallaxSubtitleKey) &&
                    <Text style={styles.parallaxSubtitle}>{item[this.props.parallaxSubtitleKey]}</Text>
                }
              </Parallax.Image>
            ))
          }
        </Parallax.ScrollView>
      )
    } else if(this.props.isFetching) {
      return (
        <View style={{alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
          { this.props.loadingType === 'dot' &&
            <DotIndicator color={Colors.theme} count={4} size={13} style={{}} />
          }
          { this.props.loadingType === 'wave' &&
            <WaveIndicator color={Colors.theme} count={4} size={80} style={{}} />
          }
          { this.props.loadingType === 'pacman' &&
            <PacmanIndicator color={Colors.theme} count={4} size={100} style={{}} />
          }
        </View>
      )
    } else if(!this.props.isFetching && (!_.isNull(this.props.errorInfo) || !_.isNull(this.state.error_title) || this.state.is_empty)) {
      return (
        <ScrollView
          style={{flexGrow:1, backgroundColor:'transparent'}}
          contentContainerStyle={{flexGrow:1, alignItems:'center', justifyContent:'center'}}>
          <View style={{flexGrow:1, alignItems:'center', justifyContent:'flex-end', backgroundColor:'transparent'}}>
            <FlexImage
              style={{flexGrow:0.4}}
              source={this.state.is_empty ? Images.info : Images.fail} />
          </View>
          <View style={{flexGrow:7, marginTop:10, paddingBottom:10, backgroundColor:'transparent'}}>
            <CollapseView
              style={{flexGrow:1}}
              collapse={false}
              renderView={(collapse) => (
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                  <Text>{this.state.error_title}</Text>
                  { !_.isEmpty(this.props.errorInfo) &&
                    <Image source={Images.down} style={{marginLeft:5, width:20, height:20}} />
                  }
                </View>
              )}
              renderCollapseView={(collapse) => {
                if(_.isEmpty(this.props.errorInfo)) return (<View></View>)
                else return (
                  <View style={{height:deviceHeight, flexGrow:1, backgroundColor:'transparent'}}>
                    <Text style={{margin:10, padding:10, backgroundColor:'white'}}>{this.props.errorInfo}</Text>
                  </View>
                )
              }}
            />
          </View>
        </ScrollView>
        )
      } else {
        return (null)
      }
  }
}

const styles = StyleSheet.create({
  parallaxImage: {
    "width": deviceWidth,
    "height": 200,
    "borderBottomWidth": 1,
    "borderBottomColor": "#fff"
  },
  parallaxOverlay: {
    "alignItems": "center",
    "justifyContent": "center",
    "backgroundColor": "rgba(0,0,0,0.3)"
  },
  parallaxTitle: {
    "fontSize": 18,
    "textAlign": "center",
    "lineHeight": 25,
    "fontWeight": "400",
    "color": "white",
    "shadowOffset": {width: 0, height: 0},
    "shadowRadius": 1,
    "shadowColor": "black",
    "shadowOpacity": 0.8,
    "fontFamily": 'Helvetica'
  },
  parallaxSubtitle: {
    "opacity": 0.9,
    "fontSize": 13,
    "textAlign": "center",
    "color": "white",
    "paddingTop": 6
  }
});

ListFilter.propTypes = {
  /*
  * isMultiSection is false => [{},{},...,{}]
  * isMultiSection is true =>
  * {[
  *   {title: 'Title1', data: ['item1', 'item2']},
  *   {title: 'Title2', data: ['item3', 'item4']},
  *   {title: 'Title3', data: ['item5', 'item6']},
  * ]}
  *
  * khusus parallax:
  * data [{..., uri:xxx},{..., uri:xxx}] => uri is background image source
  */
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  /*
   * tipe tampilan list: section atau parallax
  */
  type: PropTypes.string,
  isFetching: PropTypes.bool,
  isDataEmpty: PropTypes.bool,
  errorTitle: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
  errorInfo: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
  renderRow: PropTypes.func,
  renderHeader: PropTypes.func,
  keys: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object ]),
  /*
   * parameter pencarian data yg fleksibel
   * update state ini di parent utk filter data scr otomatis
   * format filter => [{column: 'JUDUL OBJECT YG DICARI', type: 'text/dropdown'. value: 'APA YANG DICARI'}]
  */
  filterParam: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  isHorizontal: PropTypes.bool,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  styleContent: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  isScrollOnRefresh: PropTypes.bool,
  isShowScroll: PropTypes.bool,
  isMultiSection: PropTypes.bool,
  loadingType: PropTypes.string,

  // parallax only
  onItemClicked: PropTypes.func,
  parallaxBaseImageUri: PropTypes.string, // background image base url
  parallaxImageKey: PropTypes.string, // identify on which image uri is stored on data object
  parallaxTitleKey: PropTypes.string, // identify on which title is stored on data object
  parallaxSubtitleKey: PropTypes.string, // identify on which sub-title is stored on data object
}

ListFilter.defaultProps = {
  type: 'section',
  keys: null,
  isHorizontal: false,
  isScrollOnRefresh: false,
  isMultiSection: false,
  loadingType: 'dot',
  parallaxImageKey: null,
  parallaxTitleKey: null,
  parallaxSubtitleKey: null,
  isShowScroll: false,
}

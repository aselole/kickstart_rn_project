import React, {Component} from "react"
import { View, Text, Picker, StyleSheet, Dimensions, TouchableOpacity} from "react-native"
import PropTypes from 'prop-types'
import { Colors, Images, Servers, Strings } from '../Themes/'
import stylesTheme, { deviceHeight, deviceWidth } from '../Themes/StyleSub'
const {width} = Dimensions.get("window")

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class PickerLoading extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{marginTop:20}}>
        <Text>{this.props.title}</Text>
        { !this.props.isLoading &&
          <Picker selectedValue={this.props.value}
            prompt={this.props.prompt}
            onValueChange={this.props.onValueChange}>
              { this.props.data }
          </Picker>
        }
        { this.props.isLoading &&
          <View>
            <DotIndicator color={Colors.theme} count={4} size={10}  />
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonRoundBlue: {
    backgroundColor: Colors.theme,
    paddingTop: 12,
    paddingRight: 12,
    paddingBottom: 12,
    paddingLeft: 12,
    borderColor: "transparent",
    alignSelf: "center",
    borderRadius: 20,
    marginTop: 12,
    marginLeft: 10,
    marginRight: 10,
    width: width - 30,
    elevation: 6
  },
  buttonRectangularText: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 16,
  },
})

PickerLoading.propTypes = {
  style: PropTypes.object,
  title: PropTypes.string,
  prompt: PropTypes.string,
  value: PropTypes.string,
  onValueChange: PropTypes.func,
  data: PropTypes.array,
  isLoading: PropTypes.bool
};

PickerLoading.defaultProps = {isLoading: false};

import React, { Component } from 'react'
import { StyleSheet, View, Picker, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Metrics } from '../Themes/'
import stylesTheme from '../Themes/StyleSub'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class BoxPicker extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
      <TouchableOpacity style={[ stylesTheme.itemWhiteBox ]} >
        <View style={styles.detailItemBox}>
          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={this.props.imgLeft} style={{width:30, height:30}} />
            </View>
            <View style={{ width: 0.7, height: 25, backgroundColor: 'rgb(229,229,229)'}} />
          </View>
          <View style={styles.centerInfo}>
            <Text style={[ stylesTheme.smallText, stylesTheme.greyColor, stylesTheme.light, {lineHeight: 10, marginLeft: -2.5 } ]}>
              {this.props.title}
            </Text>
            <Picker
                style={{backgroundColor: Colors.transparent, marginTop: -9, marginLeft: -10, marginRight: -3, padding: 0}}
                selectedValue={this.props.selectedValue}
                prompt={this.props.prompt}
                onValueChange={this.props.onSelected}>
                  { this.props.data.map((item, key) => (
                      <Picker.Item
                        label={item.NAMA}
                        value={item.ID}
                        key={item.ID} />
                    ))
                  }
            </Picker>
          </View>
          <TouchableOpacity style={styles.rightCol}>
            <Image source={this.props.imgRight} style={{width:35, height:35}} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 19,
    paddingTop: 10,
    paddingBottom: 7
  },
  highlightBox: {
    borderRadius: 12,
  },
  centerInfo: {
    flex: 1,
  },
  leftCol: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 61,
  },
  icon: {
    width: 41,
    alignSelf: 'center'
  },
  rightCol: {
    width: 20,
    alignItems: 'center',
  },
});

BoxPicker.propTypes = {
  title: PropTypes.string,
  imgLeft: PropTypes.number,
  imgRight: PropTypes.number,
  selectedValue: PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
  prompt: PropTypes.string,
  onSelected: PropTypes.func,
  data: PropTypes.array,
  dataLabel: PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
  dataValue: PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
  dataKey: PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
};
